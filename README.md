#Instalação

---

```shell
#Obter o gerenciador de pacotes do python
sudo apt-get install python-pip python-dev build-essential
sudo pip install --upgrade pip
sudo pip install --upgrade virtualenv

#Instalar o mkdocs
pip install mkdocs

```

Para iniciar o ambiente de desenvolvimento.
Irá levantar um servidor na porta 8000

```shell
python -m mkdocs serve
```

Para gerar o site da documentação

```shell
python -m mkdocs build
```
