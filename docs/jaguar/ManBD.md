#ManBD

Essa classe tem como intuito realizar a persistência, alteração, exclusão e pesquisa a partir de objetos mapeados conforme o banco de dados.

##Principais Atributos

| Atributo            | Tipo    | Valor padrão      | Descrição                                                            |
| ------------------- | :-----: | :---------------: | -------------------------------------------------------------------- |
| objErro             | object  |                   | Armazena a referência de um objeto Erro                              |
| ultimoObjetoUpdate  | object  |                   | Armazena o último objeto alterado                                    |
| valuesUpdate        | array   | []                | **VERIFICAR**                                                        |
| throwExceptions     | boolean | false             | Controla o uso exceções ao ocorrer erros nos métodos                 |
| mostrarErro         | boolean | true              | Controla a exibição mensagens de erro dentro da classe               |
| idRodarGatilhos     | boolean | []                | Controla o uso de gatilhos existentes dentro das classes manipuladas |

##Principais Métodos

###__construct

Construtor da classe

| Parâmetros                                           | Tipo    | Valor padrão      | Descrição                            |
| ---------------------------------------------------- | :-----: | :---------------: | ------------------------------------ |
| <span style="color: #4169E1;">Erro</span>            | object  |                   | Referência de um objeto Erro         |
| <span style="color: #4169E1;">throwExceptions</span> | boolean | false             | Controla o disparo de exceções       |
| <span style="color: #4169E1;">mostrarErro</span>     | boolean | true              | Controla a exibição de erros         |

###salvar

Persiste um objeto passado por parâmetro.

| Parâmetros                                            | Tipo    | Valor padrão      | Descrição                                                 |
| ----------------------------------------------------- | :-----: | :---------------: |---------------------------------------------------------- |
| <span style="color: #4169E1;">obj</span>              | object  |                   | Referência de um objeto a se persistido                   |
| <span style="color: #4169E1;">excludedTriggers</span> | array   | []                | Gatilhos que **não** serão rodados no momento da inserção |

###excluir

Exclui um objeto passado por parâmetro.

| Parâmetros                                            | Tipo    | Valor padrão      | Descrição                                                 |
| ----------------------------------------------------- | :-----: | :---------------: |---------------------------------------------------------- |
| <span style="color: #4169E1;">obj</span>              | object  |                   | Referência de um objeto a se persistido                   |
| <span style="color: #4169E1;">excludedTriggers</span> | array   | []                | Gatilhos que **não** serão rodados no momento da exclusão |

###PopulaObjetoRs

Popula e retorna um objeto, ou array de objetos, a partir de um [rs](../../jaguar/JDBConn).

| Parâmetros                                   | Tipo    | Valor padrão      | Descrição                                                                       |
| -------------------------------------------- | :-----: | :---------------: |-------------------------------------------------------------------------------- |
| <span style="color: #4169E1;">arr_obj</span> | array   | []                | Array/objeto que será populado a partir dos resultados do [rs](../../jaguar/JDBConn) |
| <span style="color: #4169E1;">rs</span>      | object  |                   | Objeto resultado de uma pesquisa utilizando [JDBConn](../../jaguar/JDBConn)          |

###PopulaObjetoManutencao

Popula um objeto do tipo [JMaintenance](../../jaguar/JMaintenance) a partir de uma classe.

| Parâmetros                                       | Tipo    | Valor padrão      | Descrição                                                                   |
| ------------------------------------------------ | :-----: | :---------------: |---------------------------------------------------------------------------- |
| <span style="color: #4169E1;">obj</span>         | object  |                   | Referência de um objeto utilizado como base da manutenção                   |
| <span style="color: #4169E1;">man</span>         | object  |                   | Referência de um objeto [JMaintenance](../../jaguar/JMaintenance) a ser populado |
| <span style="color: #4169E1;">extra_join</span>  | string  | null              | Bloco de SQL para fazer restrições através da cláusula JOIN                 |
| <span style="color: #4169E1;">extra_where</span> | string  | null              | Bloco de SQL para fazer restrições através da cláusula WHERE                |

###PopulaObjetoGenerico

Popula e retorna um objeto qualquer levando em consideração seus atributos definidos.

| Parâmetros                                          | Tipo             | Valor padrão      | Descrição                                                                              |
| --------------------------------------------------- | :--------------: | :---------------: |--------------------------------------------------------------------------------------- |
| <span style="color: #4169E1;">obj</span>            | object           |                   | Referência do objeto a ser populado                                                    |
| <span style="color: #4169E1;">arr_where</span>      | array / boolean  | null              | Array com restrições que serão contruídas utilizando a cláusula WHERE                  |
| <span style="color: #4169E1;">extra_where</span>    | string /boolean  | false             | Bloco de SQL para fazer restrições através da cláusula WHERE                           |
| <span style="color: #4169E1;">lazy</span>           | boolean          | true              | Controla o processo de poular um objeto dentro do objeto que está sendo populado       |
| <span style="color: #4169E1;">extraWhereLazy</span> | array            | []                | Array com restrições que serão contruídas utilizando a cláusula WHERE nos objetos lazy |


