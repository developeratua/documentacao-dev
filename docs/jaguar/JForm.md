#JForm

Cria um formulário.
Extende a classe [JFormObject](../../jaguar/JTable).

##Principais Atributos

| Atributo           | Tipo    | Valor padrão        | Descrição                                                                                                     |
| ------------------ | :-----: | :-----------------: | ------------------------------------------------------------------------------------------------------------- |
| mAction            | string  |                     | Define a ação do formulário                                                                                   |
| mMethod            | string  |                     | Define o método da ação do formulário                                                                         |
| mFocus             | boolean |                     | Define se o formulário deve ser focado na sua criação                                                         |
| mFocusedField      | string  |                     | Armazena o nome do objeto que vai receber o fóco                                                              |
| mHasPassword       | array   | []                  | Controla o uso de campos de password no formlário                                                             |
| mSubmitted         | boolean |                     | Controla a submissão do formulário                                                                            |
| mAlreadySubmitted  | object  |                     | Usado pela função javascript chamada avoidMultipleRequest para verificar se o formulário atual já foi enviado |
| mFormSubmitted     | boolean |                     | Verifica se o formulário já foi submetido                                                                     |
| mExtra             | string  |                     | Armazena um bloco de html para ser usado na tag form                                                          |
| mValid             | boolean |                     | Controla se todos os campos do formulário são válidos                                                         |
| mDebug             | boolean | false               | Controla o uso de debug                                                                                       |
| mTarget            | string  | "_self"             | Armazena o conteúdo do atributo target do formulário                                                          |
| mFunctions         | array   | []                  | Armazena as funções que serão chamadas antes da submissão do formulário                                       |
| mParameters        | array   | []                  | Armazena os parâmetros das funções que serão chamadas antes da submissão do formulário                        |
| mEnterHasActions   | boolean | true                | Define se a tecla "enter" possui alguma ação no forumlário                                                    |
| mTestOnBlur        | boolean | true                | Define se o componente vai ter seu valor testado no evento onBlur                                             |
| mEnctype           | string  | multipart/form-data | Define  o Enctype do formulário                                                                               |

##Principais Métodos

###JForm

Construtor da classe

| Parâmetros                                | Tipo    | Valor padrão      | Descrição                            |
| ----------------------------------------- | :-----: | :---------------: | ------------------------------------ |
| <span style="color: #4169E1;">name</span> | string  | false             |Define o nome do objeto               |

###SetTarget

Define o atributo "target" da tag "form".

| Parâmetros                                       | Tipo    | Valor padrão      | Descrição           |
| ------------------------------------------------ | :-----: | :---------------: |-------------------- |
| <span style="color: #4169E1;">target</span>      | string  |                   | Atributo target     |

###SetName

Define o atributo "name" da tag "form".

| Parâmetros                                    | Tipo    | Valor padrão      | Descrição       |
| --------------------------------------------- | :-----: | :---------------: |---------------- |
| <span style="color: #4169E1;">name</span>     | string  | "form"            | Atributo name   |

###SetAction

Define o destino dos dados do formulário.

| Parâmetros                                    | Tipo    | Valor padrão         | Descrição       |
| --------------------------------------------- | :-----: | :------------------: |---------------- |
| <span style="color: #4169E1;">action</span>   | string  | $_SERVER["PHP_SELF"] | Link destino    |

###SetMethod

Define a forma de envio dos dados.

| Parâmetros                                    | Tipo    | Valor padrão      | Descrição       |
| --------------------------------------------- | :-----: | :---------------: |---------------- |
| <span style="color: #4169E1;">method</span>   | string  | "POST"            | Atributo method |

###AddFunction

Adiciona funções JS para serem executadas no momento da submissão do formulário.

| Parâmetros                                    | Tipo    | Valor padrão      | Descrição       |
| --------------------------------------------- | :-----: | :---------------: |---------------- |
| <span style="color: #4169E1;">function</span> | string  | false             | Nome da função  |

###AddParameter

Adiciona os parâmetros das funções JS para serem executadas no momento da submissão do formulário.

| Parâmetros                                     | Tipo    | Valor padrão  | Descrição           |
| ---------------------------------------------- | :-----: | :-----------: |-------------------- |
| <span style="color: #4169E1;">function</span>  | string  | false         | Nome da função      |
| <span style="color: #4169E1;">parameter</span> | string  | false         | Valor do parâmetro  |

###SetFocus

Define o foco para um determinado campo de formulário.

| Parâmetros                                    | Tipo    | Valor padrão      | Descrição           |
| --------------------------------------------- | :-----: | :---------------: |-------------------- |
| <span style="color: #4169E1;">object</span>   | object  | false             | Objeto a ser focado |

###SetDebug

Controla o uso do debug.

| Parâmetros                                    | Tipo    | Valor padrão      | Descrição                   |
| --------------------------------------------- | :-----: | :---------------: |---------------------------- |
| <span style="color: #4169E1;">debug</span>    | boolean | false             | Controlador do uso de debug |

###IsSubmitted

Verifica se o formulário foi submetido.

| Parâmetros                                    | Tipo    | Valor padrão      | Descrição                   |
| --------------------------------------------- | :-----: | :---------------: |---------------------------- |
| <span style="color: #4169E1;"></span>         |         |                   |                             |

###SetEnctype

Define o enctype.

| Parâmetros                                    | Tipo    | Valor padrão      | Descrição                   |
| --------------------------------------------- | :-----: | :---------------: |---------------------------- |
| <span style="color: #4169E1;">enctype</span>  | string  |                   | Atributo enctype            |


