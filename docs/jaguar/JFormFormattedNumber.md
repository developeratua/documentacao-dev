#JFormFormattedNumber

Cria um campo de formulário que aceita apenas a entrada de números decimais.
Extende a classe [JFormText](../../jaguar/JFormText).

```html
<input class="f_number" id="2" type="text" name="f_number" value="" size="10">
```

##Principais Atributos

| Atributo           | Tipo    | Valor padrão      | Descrição                                             |
| ------------------ | :-----: | :---------------: | ----------------------------------------------------- |
| $mDigits           | int     | 2                 | Armazena o número de digitos após o separador decimal |
| $mValue1           | float   |                   | Armazena o primeiro valor para comparação             |
| $mValue2           | float   |                   | Armazena o segundo valor para comparação              |
| $mCondition        | string  |                   | Armazena o operador para comparação                   |
| $mError            | string  | "Valor Inválido!" | Armazena a mensagem de erro resultado das comparações |
| $mFormat           | string  | "pt_BR"           | Armazena a formatação do número                       |
| $mDisplaySize      | string  | "10"              | Define o máximo de digitos antes do separador decimal |
| $mNegativeNumber   | boolean | false             | Controlador de números negativos                      |

##Principais Métodos

###JFormFormattedNumber

Construtor da classe

| Parâmetros                                | Tipo    | Valor padrão      | Descrição                            |
| ----------------------------------------- | :-----: | :---------------: | ------------------------------------ |
| <span style="color: #4169E1;">name</span> | string  | false             |Define o nome do objeto               |
| <span style="color: #4169E1;">value</span>| string  | false             |Define um valor inicial para o objeto |

###SetValue1

Define o primeiro valor para comparação.

| Parâmetros                                    | Tipo    | Valor padrão      | Descrição                             |
| --------------------------------------------- | :-----: | :---------------: |-------------------------------------- |
| <span style="color: #4169E1;">value1</span>   | string  | false             | Primeiro valor de comparação          |
| <span style="color: #4169E1;">from</span>     | string  | "sys"             | Formato original do valor             |
| <span style="color: #4169E1;">tom</span>      | string  | "pt_BR"           | Formato ao qual o valor será ajustado |

###SetValue2

Define o segundo valor para comparação.

| Parâmetros                                    | Tipo    | Valor padrão      | Descrição                             |
| --------------------------------------------- | :-----: | :---------------: |-------------------------------------- |
| <span style="color: #4169E1;">value2</span>   | string  | false             | Segundo valor de comparação          |
| <span style="color: #4169E1;">from</span>     | string  | "sys"             | Formato original do valor             |
| <span style="color: #4169E1;">tom</span>      | string  | "pt_BR"           | Formato ao qual o valor será ajustado |
###SetCondition

Define o operador para comparação.

* =
* <
* <=
* \>
* \>=

| Parâmetros                                    | Tipo    | Descrição                                                                      |
| --------------------------------------------- | ------- | ------------------------------------------------------------------------------ |
| <span style="color: #4169E1;">condition</span>| string  | Operador para comparação                                                       |

###SetNegativeNumber

Define se os caracteres especiais podem ser convertidos em caracteres comuns ou não

| Parâmetros                                               | Tipo    | | Valor padrão      | Descrição                                                                      |
| -------------------------------------------------------- | :-----: | | :---------------: | ------------------------------------------------------------------------------ |
| <span style="color: #4169E1;">specialCharacter</span>    | boolean | |                   | Define se os caractéres serão convertidos ou não                               |

###SetFormat

Define o formato de apresentação dos valores.

* pt_BR

* sys

* us

| Parâmetros                                               | Tipo    | | Valor padrão      | Descrição                                                                      |
| -------------------------------------------------------- | :-----: | | :---------------: | ------------------------------------------------------------------------------ |
| <span style="color: #4169E1;">format</span>              | string  | |                   | Formato de apresentação dos dados                                              |

###SetPrecision

Define a precisão da formatação.

| Parâmetros                                  | Tipo    | Valor padrão      | Descrição                                                 |
| ------------------------------------------- | :-----: | :---------------: | --------------------------------------------------------- |
| <span style="color: #4169E1;">size</span>   | int     | 15                | Define o número de caracteres antes do separador decimal  |
| <span style="color: #4169E1;">digits</span> | int     | 2                 | Define o número de caracteres depois do separador decimal |