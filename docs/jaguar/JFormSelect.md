#JFormSelect

Cria um objeto caixa de combinação.
Extende a classe [JFormObject](../../jaguar/JFormObject).

```php
<?php
  session_start();
  require_once("lib/jaguar/jaguar.inc.php");
  require_once("include/funcoes.inc.php");

  $arrOpcoes = [
    ["description" => "Valor 2", "value" => 2],
    ["description" => "Valor 3", "value" => 3],
    ["description" => "Valor 4", "value" => 4],
    ["description" => "Valor 5", "value" => 5],
    ["description" => "Valor 6", "value" => 6],
  ];

  $cd_select = new JFormSelect("f_cd_select");
  $cd_select->SetOptions($arrOpcoes);
  $cd_select->AddOption(1, "Valor 1");
  $cd_select->SetFirstEmpty();
  $cd_select->SetFirstValue(1);
  $cd_select->SetDisplaySize(180);
```

##Principais Atributos

| Atributo           | Tipo    | Valor padrão      | Descrição                                                                                             |
| ------------------ | :-----: | :---------------: |------------------------------------------------------------------------------------------------------ |
| mOptionsIdx        | string  |                   | Controla o número de opções de uma caixa de combinação                                                |
| mOptions           | array   | []                | Armazena as opções para seleção                                                                       |
| mFirstEmpty        | boolean | false             | Controla se a primeira opção da caixa é vazia                                                         |
| mOrder             | string  |                   | Controla a ordem das opções ["value", "description"]                                                  |
| mFirstValue        | int     |                   | Controla qual opção vai ser a primeira (caso exista a primeira opção vazia, após ela)                 |

##Principais Métodos

###JFormSelect

Construtor da classe

| Parâmetros                                | Tipo    | Valor padrão      | Descrição                             |
| ----------------------------------------- | :-----: | :---------------: | ------------------------------------- |
| <span style="color: #4169E1;">name</span> | string  |                   | Define o nome do objeto               |
| <span style="color: #4169E1;">value</span>| string  | false             | Define um valor inicial para o objeto |

###SetDisplaySize

Define, em pixels, a largura e a altura do campo.

| Parâmetros                                      | Tipo    | Valor padrão | Descrição        |
| ----------------------------------------------- | :-----: | :----------: | ---------------- |
| <span style="color: #4169E1;">sizeWidth</span>  | int     | 100          | Valor da largura |
| <span style="color: #4169E1;">sizeHeight</span> | int     | null         | Valor da altura  |

###AddOption

Adiciona opções na caixa de combinação de forma manual, informando o valor e a descrição, respectivamente.

| Parâmetros                                 | Tipo    | Valor padrão | Descrição           |
| ------------------------------------------ | :-----: | :----------: | ------------------- |
| <span style="color: #4169E1;">value</span> | string  |              | Valor da opção      |
| <span style="color: #4169E1;">text</span>  | string  | false        | Descrição da opção  |

###SetOptions

Adiciona opções na caixa de combinação a partir de um array associativo contendo as chaves "value" e "key".

| Parâmetros                               | Tipo  | Valor padrão | Descrição                                                       |
| ---------------------------------------- | :---: | :----------: | --------------------------------------------------------------- |
| <span style="color: #4169E1;">arr</span> | array | []           | Array associativo contendo as opções para a caixa de combinação |

###SetOrder

Define a ordem de exibição das opções da caixa de combinações. Essas podendo ser exibidas por "descrição" ou "valor"

| Parâmetros                                 | Tipo   | Valor padrão  | Descrição                    |
| ------------------------------------------ | :----: | :-----------: | ---------------------------- |
| <span style="color: #4169E1;">order</span> | string | "description" | Controle da ordem das opções |

###SetFirstValue

Define qual será a primeira opção a ser exibida. Se estiver habilitado para exibir a primeira opção vazia, [SetFirstEmpty](../../jaguar/JFormSelect#setfirstempty), essa será a segunda opção.

| Parâmetros                                 | Tipo   | Valor padrão  | Descrição                    |
| ------------------------------------------ | :----: | :-----------: | ---------------------------- |
| <span style="color: #4169E1;">value</span> | int    |               | Valor da opção               |

###SetFirstEmpty

Define que a primeira opção será vazia.

| Parâmetros                                 | Tipo    | Valor padrão  | Descrição                    |
| ------------------------------------------ | :-----: | :-----------: | ---------------------------- |
| <span style="color: #4169E1;">bool</span>  | boolean | true          | Valor da opção               |

