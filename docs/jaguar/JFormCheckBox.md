#JFormCheckBox

Cria uma caixa de seleção para formulário.
Extende a classe [JFormObject](../../jaguar/JFormObject).

##Principais Atributos

| Atributo           | Tipo    | Valor padrão      | Descrição                                             |
| ------------------ | :-----: | :---------------: | ----------------------------------------------------- |
| mDescription       | string  |                   | **VERIFICAR**                                         |
| mOptionsIdx        | float   |                   | **VERIFICAR**                                         |
| mOptionsChecked    | float   |                   | **VERIFICAR**                                         |
| mOptions           | array   | []                | **VERIFICAR**                                         |

##Principais Métodos

###JFormCheckBox

Construtor da classe

| Parâmetros                                | Tipo    | Valor padrão      | Descrição                            |
| ----------------------------------------- | :-----: | :---------------: | ------------------------------------ |
| <span style="color: #4169E1;">name</span> | string  | false             |Define o nome do objeto               |
| <span style="color: #4169E1;">value</span>| string  | false             |Define um valor inicial para o objeto |

###SetDescription

Define a descrição do label

| Parâmetros                                       | Tipo    | Valor padrão      | Descrição           |
| ------------------------------------------------ | :-----: | :---------------: |-------------------- |
| <span style="color: #4169E1;">description</span> | string  |                   | Descrição do label  |

###SetChecked

Define como "marcadas" as opções que foram selecionadas

| Parâmetros                                    | Tipo    | Valor padrão      | Descrição       |
| --------------------------------------------- | :-----: | :---------------: |---------------- |
| <span style="color: #4169E1;">value2</span>   | array   | []                | Array de opções |
