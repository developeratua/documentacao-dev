#JFormNumber

Cria um campo de formulário que aceita apenas a entrada de números em ponto flutuânte.
Extende a classe [JFormText](../../jaguar/JFormText).

```html
<input class="f_number" id="2" type="text" name="f_number" value="" size="10">
```

##Principais Atributos

| Atributo           | Tipo              | Valor padrão      | Descrição                                                                                             |
| ------------------ | :---------------: | :---------------: |------------------------------------------------------------------------------------------------------ |
| $mValue1           | float             |                   | Armazena o primeiro valor para comparação                                                             |
| $mValue2           | float             |                   | Armazena o segundo valor para comparação                                                              |
| $mCondition        | string            |                   | Armazena o operador para comparação                                                                   |
| $mError            | string            | "Valor Inválido!" | Armazena a mensagem de erro resultado das comparações                                                 |
| $mNegativeNumber   | boolean           | false             | Controlador de números negativos                                                                      |

##Principais Métodos

###JFormNumber

Construtor da classe

| Parâmetros                                | Tipo    | Valor padrão      | Descrição                            |
| ----------------------------------------- | ------- | :---------------: | ------------------------------------ |
| <span style="color: #4169E1;">name</span> | string  |                   | Define o nome do objeto              |
| <span style="color: #4169E1;">value</span>| string  | false             | Define um valor inicial para o objeto|

###SetValue1

Define o primeiro valor para comparação.

| Parâmetros                                    | Tipo    | Valor padrão | Descrição                         |
| --------------------------------------------- | ------- | :----------: | --------------------------------- |
| <span style="color: #4169E1;">value1</span>   | string  | false        | Primeiro valor de comparação      |

###SetValue2

Define o segundo valor para comparação.

| Parâmetros                                    | Tipo    | Valor padrão | Descrição                         |
| --------------------------------------------- | ------- | :----------: | --------------------------------- |
| <span style="color: #4169E1;">value2</span>   | string  | false        | Segundo valor de comparação       |

###SetCondition

Define o operador para comparação.

* =
* <
* <=
* \>
* \>=

| Parâmetros                                    | Tipo    | Valor padrão | Descrição                                                                      |
| --------------------------------------------- | ------- | :----------: | ------------------------------------------------------------------------------ |
| <span style="color: #4169E1;">condition</span>| string  | false        | Operador para comparação                                                       |

###SetNegativeNumber

Define se o objeto pode receber numeros negativos.

| Parâmetros                                    | Tipo    | Valor padrão | Descrição                                                                      |
| --------------------------------------------- | ------- | :----------: | ------------------------------------------------------------------------------ |
| <span style="color: #4169E1;">value</span>    | boolean | 1            | Controlador de entrada de números negativos                                    |

