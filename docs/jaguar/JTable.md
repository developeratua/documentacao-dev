#JTable

Cria uma tabela.
Extende a classe [JFormObject](../../jaguar/JFormObject).

##Principais Atributos

| Atributo           | Tipo    | Valor padrão | Descrição                                                                  |
| ------------------ | :-----: | :----------: | -------------------------------------------------------------------------- |
| mAlign             | string  | false        | Armazena o alinhamento da tabela                                           |
| mBorder            | int     | false        | Armazena a estilização da borda da tabela                                  |
| mWidth             | int     | false        | Armazena a largura da tabela                                               |
| mCellPadding       | int     | false        | Armazena o padding da célula da tabela                                     |
| mCellSpacing       | int     | false        | Armazena o espaçamento da célula da tabela                                 |
| mTheadOpened       | boolean | false        | Controla abertura e fechamento do THead                                    |
| mTbodyOpened       | boolean | false        | Controla abertura e fechamento do TBody                                    |
| mRowOpened         | boolean | false        | Controla abertura e fechamento do tr                                       |
| mHeaderOpened      | boolean | false        | Controla abertura e fechamento do th                                       |
| mCellOpened        | boolean | false        | Controla abertura e fechamento do td                                       |
| mTableOpened       | boolean | false        | Controla abertura e fechamento do table                                    |
| mStriped           | boolean | false        | Controla o uso de tabela listrada                                          |
| mRowNumber         | int     | 0            | Armazena o nº da linha atual (quando mStriped = true)                      |
| mIndentation       | int     | 0            | Armazena o nº de caracteres que a tabela deve ser recuada                  |
| mSpace             | string  |              | Armazena o recuo completo                                                  |
| mTightCell         | boolean |              | Controla o uso de espaços entre tags de células                            |
| mTightCell         | boolean | false        | Controla o uso de espaços entre tags de células                            |
| mRowOdd            | string  | "rowodd"     | Armazena a classe CSS usada para tabelas listrada, linha ímpar             |
| mRowOddHi          | string  | "rowodd-hi"  | Armazena a classe CSS usada para tabelas listrada, linha ímpar (highlight) |
| mRowEven           | string  | "roweven"    | Armazena a classe CSS usada para tabelas listrada, linha par               |
| mRowOddHi          | string  | "roweven-hi" | Armazena a classe CSS usada para tabelas listrada, linha par (highlight)   |
| mTableOptions      | array   | []           | Armazena as propriedades da tabela                                         |
| mLastHeader        |         |              | Armazena o texto do último header                                          |

##Principais Métodos

###JTable

Construtor da classe.

| Parâmetros                                   | Tipo    | Valor padrão      | Descrição                            |
| -------------------------------------------- | :-----: | :---------------: | ------------------------------------ |
| <span style="color: #4169E1;">options</span> | array   | false             |Define as propriedades do objeto      |

###SetTableOptions

Define as opções a serem utilizadas no objeto tabela.

| Parâmetros                                   | Tipo    | Valor padrão      | Descrição                                  |
| -------------------------------------------- | :-----: | :---------------: |------------------------------------------- |
| <span style="color: #4169E1;">options</span> | array   | false             | Array associativo com as opções da tabela  |

###SetAlign

Define o alinhamento da tabela.

| Parâmetros                                 | Tipo    | Valor padrão | Descrição              |
| ------------------------------------------ | :-----: | :----------: |----------------------- |
| <span style="color: #4169E1;">align</span> | string  |              | Posição de alinhamento |

###SetIndentation

Define a identação das tags do objeto tabela.

| Parâmetros                                  | Tipo    | Valor padrão | Descrição                                                   |
| ------------------------------------------- | :-----: | :----------: |------------------------------------------------------------ |
| <span style="color: #4169E1;">length</span> | int     |              | Número de espaços em branco antes de qualquer tag da tabela |

###SetBorder

Define a utilização de borda na tabela.

| Parâmetros                                  | Tipo    | Valor padrão | Descrição                                                   |
| ------------------------------------------- | :-----: | :----------: |------------------------------------------------------------ |
| <span style="color: #4169E1;">length</span> | int     | false        | Controlador de utilização de bordas                         |

###SetWidth

Define a largura da tabela.

| Parâmetros                                  | Tipo    | Valor padrão | Descrição                                           |
| ------------------------------------------- | :-----: | :----------: |---------------------------------------------------- |
| <span style="color: #4169E1;">width</span>  | int     | false        | Largura da tabela em px                             |

###SetCellPadding

Define o padding das células da tabela.

| Parâmetros                                        | Tipo    | Valor padrão | Descrição                                           |
| ------------------------------------------------- | :-----: | :----------: |---------------------------------------------------- |
| <span style="color: #4169E1;">cellPadding</span>  | int     | false        | Padding das células da tabela em px                 |

###SetCellSpacing

Define o espaçamento das células da tabela.

| Parâmetros                                        | Tipo    | Valor padrão | Descrição                                           |
| ------------------------------------------------- | :-----: | :----------: |---------------------------------------------------- |
| <span style="color: #4169E1;">cellspacing</span>  | int     | false        | Espaçamento das células da tabela em px             |

###SetStriped

Define a utilização da tabela listrada.

| Parâmetros                                        | Tipo    | Valor padrão | Descrição                                           |
| ------------------------------------------------- | :-----: | :----------: |---------------------------------------------------- |
| <span style="color: #4169E1;">striped</span>      | boolean | true         | Controlador da utilização da tabela listrada        |

###SetTightCell

Define o espaçamento entre as tags das células.

| Parâmetros                                        | Tipo    | Valor padrão | Descrição                                                                 |
| ------------------------------------------------- | :-----: | :----------: |-------------------------------------------------------------------------- |
| <span style="color: #4169E1;">tight</span>        | boolean | true         | Controlador da utilização de espaçamento entre as tag de célula na tabela |

###SetLineStyles

Quando a tabela for listrada, [SetStriped](../../jaguar/JTable#setstriped), define o estilo das linhas.

| Parâmetros                                        | Tipo    | Valor padrão | Descrição                                                                 |
| ------------------------------------------------- | :-----: | :----------: |-------------------------------------------------------------------------- |
| <span style="color: #4169E1;">rowOdd</span>       | String  |              | Define a classes a ser usada nas linhas ímpares                           |
| <span style="color: #4169E1;">rowOddHi</span>     | String  |              | Define a classes a ser usada nas linhas ímpares highlight                 |
| <span style="color: #4169E1;">rowEven</span>      | String  |              | Define a classes a ser usada nas linhas pares                             |
| <span style="color: #4169E1;">rowEvenHi</span>    | String  |              | Define a classes a ser usada nas linhas pares highlight                   |

###OpenTable

Define a abertura da tag "table".

| Parâmetros                                        | Tipo    | Valor padrão | Descrição                                                                 |
| ------------------------------------------------- | :-----: | :----------: |-------------------------------------------------------------------------- |
| <span style="color: #4169E1;">options</span>      | array   | false        | Define opções da tabela                                                   |

###CloseTable

Define o fechamento da tag "table".

| Parâmetros                                        | Tipo    | Valor padrão | Descrição                                                                 |
| ------------------------------------------------- | :-----: | :----------: |-------------------------------------------------------------------------- |
| <span style="color: #4169E1;"></span>             |         |              |                                                                           |

###OpenThead

Define a abertura da tag "thead".

| Parâmetros                                        | Tipo    | Valor padrão | Descrição                                                                 |
| ------------------------------------------------- | :-----: | :----------: |-------------------------------------------------------------------------- |
| <span style="color: #4169E1;"></span>             |         |              |                                                                           |

###CloseThead

Define o fechamento da tag "thead".

| Parâmetros                                        | Tipo    | Valor padrão | Descrição                                                                 |
| ------------------------------------------------- | :-----: | :----------: |-------------------------------------------------------------------------- |
| <span style="color: #4169E1;"></span>             |         |              |                                                                           |

###OpenTbody

Define a abertura da tag "thead".

| Parâmetros                                        | Tipo    | Valor padrão | Descrição                                                                 |
| ------------------------------------------------- | :-----: | :----------: |-------------------------------------------------------------------------- |
| <span style="color: #4169E1;"></span>             |         |              |                                                                           |

###CloseTbody

Define o fechamento da tag "thead".

| Parâmetros                                        | Tipo    | Valor padrão | Descrição                                                                 |
| ------------------------------------------------- | :-----: | :----------: |-------------------------------------------------------------------------- |
| <span style="color: #4169E1;"></span>             |         |              |                                                                           |

###OpenRow

Define a abertura da tag "tr".

| Parâmetros                                        | Tipo    | Valor padrão | Descrição                                                                 |
| ------------------------------------------------- | :-----: | :----------: |-------------------------------------------------------------------------- |
| <span style="color: #4169E1;">options</span>      | array   | false        | Define opções da tabela                                                   |

###CloseRow

Define o fechamento da tag "tr".

| Parâmetros                                        | Tipo    | Valor padrão | Descrição                                                                 |
| ------------------------------------------------- | :-----: | :----------: |-------------------------------------------------------------------------- |
| <span style="color: #4169E1;"></span>             |         |              |                                                                           |

###OpenHeader

Define a abertura da tag "th".

| Parâmetros                                        | Tipo    | Valor padrão | Descrição                                                                 |
| ------------------------------------------------- | :-----: | :----------: |-------------------------------------------------------------------------- |
| <span style="color: #4169E1;">text</span>         | string  |              | Define o conteúdo dentro do header                                        |
| <span style="color: #4169E1;">options</span>      | array   | false        | Define opções da tabela                                                   |

###CloseRow

Define o fechamento da tag "th".

| Parâmetros                                        | Tipo    | Valor padrão | Descrição                                                                 |
| ------------------------------------------------- | :-----: | :----------: |-------------------------------------------------------------------------- |
| <span style="color: #4169E1;"></span>             |         |              |                                                                           |

###OpenCell

Define a abertura da tag "td".

| Parâmetros                                        | Tipo    | Valor padrão | Descrição                                                                 |
| ------------------------------------------------- | :-----: | :----------: |-------------------------------------------------------------------------- |
| <span style="color: #4169E1;">text</span>         | string  |              | Define o conteúdo dentro da célula                                        |
| <span style="color: #4169E1;">options</span>      | array   | false        | Define opções da tabela                                                   |

###CloseCell

Define o fechamento da tag "td".

| Parâmetros                                        | Tipo    | Valor padrão | Descrição                                                                 |
| ------------------------------------------------- | :-----: | :----------: |-------------------------------------------------------------------------- |
| <span style="color: #4169E1;"></span>             |         |              |                                                                           |