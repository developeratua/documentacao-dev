#JFormObject

Classe base para a criação de objetos de formulário.
Extende a classe [JPrimitiveObject](../../jaguar/JPrimitiveObject).

```html
<input class="f_nm_objeto" id="1" type="text" name="f_nm_objeto" value="" size="40"/>
```

##Principais Atributos

| Atributo                | Funcionamento                                                                                                                                                        |
| ----------------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| $mValue                 | Armazena o valor do objeto                                                                                                                                           |
| $mHasMaxLength          | Controla se o objeto vai ter um tamanho máximo                                                                                                                       |
| $mSize                  | Armazena o tamanho do objeto                                                                                                                                         |
| $mMaxLength             | Armazena o máximo de caracteres que o objeto irá suportar                                                                                                            |
| $mDisabled              | Controla se o objeto será desativado													                                                                                                       |
| $mTestIfEmpty           | Controla a verificação do preenchimento do objeto <span style="color: #2E8B57;">[0: Não - 1: Quando submeter o formulário - 2: Nos eventos onBlur e onSubmit]</span> |
| $mTestIfEmptyMessage    | Armazena a mensagem de erro de preenchimento                                                                                                                         |
| $mExtra                 | Armazena um código HTML extra na tag input                                                                                                                           |
| $mAlign 					      | Armazena o alinhamento do objeto                                                                                                                                     |
| $mEmpty 					      | Verifica se objeto possui valor                                                                                                                                      |
| $mCssclass  			      | Armazena uma classe CSS do objeto                                                                                                                                    |
| $mEvents  			        | Armazena O nome das funções associadas aos eventos JS                                                                                                                |
| $mForceDataTypeJS       | **VERIFICAR**                                                                                                                                                        |
| $mParameters            | Armazena os parâmetros das funções associadas aos eventos JS                                                                                                         |
| $mPopUpUrl              | Controla o uso de popup                                                                                                                                              |
| $mPopUpHeight           | Define a altura do popup                                                                                                                                             |
| $mPopUpLabel            | Define o link do popup                                                                                                                                               |
| $mLinkImage             | Armazena as imagens usadas no link do popup                                                                                                                          |
| $mUseLinkImage          | Controla o uso de imagens no link do popup                                                                                                                           |
| $mAllowInvalidSubmission| Controla o envio do formulário com esse campo inválido                                                                                                               |
| $mGridParameters        | Controla se o objeto requer parâmetros especiais na chamada SetValue do grid                                                                                         |
| $mTip                   | Armazena um objeto Tipo                                                                                                                                              |
| $mInvalidMessage        | Armazena a mensagem de erro caso ocorra uma entrada de dados inválida                                                                                                |
| $mMinimumRepeat         | Armazena o mínimo de vezes que uma ocorrência deve ter em uma expressão regular                                                                                      |
| $mMaximumRepeat         | Armazena o máximo de vezes que uma ocorrência deve ter em uma expressão regular                                                                                      |
| $mWidth                 | Armazena o valor de largura                                                                                                                                          |
| $mHeight                | Armazena o valor de altura                                                                                                                                           |
| $mMainForm              | Armazena a instância do objeto que contem esse objeto                                                                                                                |
| $mDefaultValue          | **VERIFICAR**                                                                                                                                                        |
| $mLabel                 | Armazena o label do objeto                                                                                                                                           |
| $mError                 | Armazena uma mensagem de erro                                                                                                                                        |
| $mUsePopUp              | Controla o uso de popup                                                                                                                                              |

##Principais Métodos

###SetName

Define o name do objeto. Ao utilizar essa função, quando o campo for gerado, os atributos name e class terão o valor passado pelo parâmetro.

| Parâmetros                                | Tipo    | Descrição                            |
| ----------------------------------------- | ------- | ------------------------------------ |
| <span style="color: #4169E1;">name</span> | string  | Name do objeto                       |

###SetLabel

Define o label do objeto.

| Parâmetros                                 | Tipo    | Descrição                            |
| ------------------------------------------ | ------- | ------------------------------------ |
| <span style="color: #4169E1;">label</span> | string  | Label do objeto                      |

###SetDefaultValue

Define um valor padrão para o objeto.

O valor passado por parâmetro deve ser válido, ou seja, não pode ser "<span style="color: #4169E1;">false</span>".

Caso seja definido um valor utilizando o método [SetValue](../../jaguar/JFormObject#setvalue), o valor padrão será sobrescrito.

| Parâmetros                                 | Tipo    | Descrição                            |
| ------------------------------------------ | ------- | ------------------------------------ |
| <span style="color: #4169E1;">value</span> | string  | Valor padrão do objeto               |

###SetValue

Atribui um valor para o objeto.

| Parâmetros                                 | Tipo    | Descrição                            |
| ------------------------------------------ | ------- | ------------------------------------ |
| <span style="color: #4169E1;">value</span> | string  | Valor a ser atribuido ao objeto      |

###SetSize

Define o tamanho do campo que será gerado pelo objeto.

Este valor é em pixels, a não ser que o objeto derivado seja text ou password. Neste caso, este valor é um número inteiro de caracteres.


| Parâmetros                                 | Tipo    | Descrição                            |
| ------------------------------------------ | ------- | ------------------------------------ |
| <span style="color: #4169E1;">size</span>  | int     | O tamanho inicial do controle        |

###SetCssSize

Define a largura e a altura do campo, através de CSS, que será gerado pelo objeto.

| Parâmetros                                 | Tipo    | Descrição                            |
| ------------------------------------------ | ------- | ------------------------------------ |
| <span style="color: #4169E1;">width</span> | string  | Define a largura em percentagem ou px|
| <span style="color: #4169E1;">height</span>| string  | Define a altura em percentagem ou px |

###SetMaxLength

Se o objeto extendido for text, email ou password, este atributo especifica o número máximo de caracteres que o usuário pode inserir; para outros tipos de objetos, este atributo é ignorado. Seu valor pode exceder o do atributo size.

| Parâmetros                                     | Tipo | Descrição                            |
| ---------------------------------------------- | ---- | ------------------------------------ |
| <span style="color: #4169E1;">maxlength</span> | int  | Número máximo de caracteres          |

###SetTestIfEmpty

Verfica se objeto possui um valor definido, caso não tenha, exibe uma mensagem de erro.

No parâmetro <span style="color: #4169E1;">test</span> é possível passar dois valores:

0 - Não realiza o teste;

1 - Realiza o teste no momento da submissão do formulário;

Verifique os [padrões de mensagem de erro](../../padroes/mensagens).

| Parâmetros                                     | Tipo    | Descrição                            |
| ---------------------------------------------- | ------- | ------------------------------------ |
| <span style="color: #4169E1;">test</span>      | int     | Controlador do teste                 |
| <span style="color: #4169E1;">message</span>   | string  | Mensagem de erro                     |

###SetExtra

Adiciona um trecho de HTML dentro da tag principal do objeto.

| Parâmetros                                     | Tipo    | Descrição                            |
| ---------------------------------------------- | ------- | ------------------------------------ |
| <span style="color: #4169E1;">code</span>      | string  | Código HTML                          |

###SetAlign

Definie a posição do alinhamento do objeto

O parâmetro <span style="color: #4169E1;">align</span> pode receber três valores:

* left;
* right;
* centrer.

| Parâmetros                                     | Tipo    | Descrição                            |
| ---------------------------------------------- | ------- | ------------------------------------ |
| <span style="color: #4169E1;">align</span>     | string  | Posição do alinhamento               |

###SetCssClass

Atribui ao objeto uma ou mais classes CSS.

| Parâmetros                                     | Tipo    | Descrição                            |
| ---------------------------------------------- | ------- | ------------------------------------ |
| <span style="color: #4169E1;">class</span>     | string  | Nome da(s) classe(s) CSS             |

###SetDisabled

Este método recebe um parâmetro booleano que indica que o objto de formulário não está disponível para interação.

| Parâmetros                                     | Tipo    | Descrição                            |
| ---------------------------------------------- | ------- | ------------------------------------ |
| <span style="color: #4169E1;">disabled</span>  | boolean | Controlador de interação             |

###SetEvents

Define um gatilho para ser chamado no momento que ocorrer um determinado evento.

| Parâmetros                                     | Tipo    | Descrição                            |
| ---------------------------------------------- | ------- | ------------------------------------ |
| <span style="color: #4169E1;">event</span>     | string  | Enveto que servirá como gatilho      |
| <span style="color: #4169E1;">function</span>  | string  | Nome da função a ser chamada         |

###SetParameters

Adiciona os parâmetros para a função definida no método [SetEvents](../../jaguar/JFormObject#setevents)

| Parâmetros                                     | Tipo    | Descrição                            |
| ---------------------------------------------- | ------- | ------------------------------------ |
| <span style="color: #4169E1;">function</span>  | string  | Nome da função definida              |
| <span style="color: #4169E1;">parameter</span> | string  | Valor do parâmetro da função         |
| <span style="color: #4169E1;">force</span>     | boolean | Força a conversão do tipo do valor   |

###IsValid

Verifica se um objeto é válido.

Esse método geralmente é sobreescrevido para abranger as restrições das classes que extendem esse objeto.

| Parâmetros                                     | Tipo    | Descrição                            |
| :--------------------------------------------: | :-----: | :----------------------------------: |
| -                                              | -       | -                                    |

###IsDisabled

Verifica se um objeto está desabilitado.

| Parâmetros                                     | Tipo    | Descrição                            |
| :--------------------------------------------: | :-----: | :----------------------------------: |
| -                                              | -       | -                                    |

###GetValue

Retorna o valor do objeto.

| Parâmetros                                     | Tipo    | Descrição                            |
| :--------------------------------------------: | :-----: | :----------------------------------: |
| -                                              | -       | -                                    |

###AddPopUpLabel

Adiciona label ao popup.

Caso o valor do label seja **Consultar** ou **Adicionar**, será atribuído a ele imagens.

| Parâmetros                                     | Tipo    | Descrição                            |
| ---------------------------------------------- | ------- | ------------------------------------ |
| <span style="color: #4169E1;">label</span>     | string  | Descrição do label                   |

###AddPopUpUrl

Adiciona link do popup.

| Parâmetros                                     | Tipo    | Descrição                                                     |
| ---------------------------------------------- | ------- | ------------------------------------------------------------- |
| <span style="color: #4169E1;">link</span>      | string  | Link para a página popup                                      |
| <span style="color: #4169E1;">resizable</span> | boolean | Define se o popup vai ajustar seu tamanho                     |
| <span style="color: #4169E1;">autoclose</span> | boolean | Define o fechamento automático do popup ao selecionar um item |

###AddPopUpField

Define o campo no formulário usado no popup.

| Parâmetros                                        | Tipo    | Descrição                                                     |
| ------------------------------------------------- | ------- | ------------------------------------------------------------- |
| <span style="color: #4169E1;">popUp</span>        | string  | Link para a página popup                                      |
| <span style="color: #4169E1;">field</span>        | string  | Name do campo no formulário                                   |
| <span style="color: #4169E1;">variableName</span> | string  | Usa o nome da variável ao invés do name do campo              |
| <span style="color: #4169E1;">fixedValue</span>   | boolean | **VERIFICAR**                                                 |

###AddPopUpField

Define o campo no formulário usado no popup.

| Parâmetros                                        | Tipo    | Descrição                                                     |
| ------------------------------------------------- | ------- | ------------------------------------------------------------- |
| <span style="color: #4169E1;">popUp</span>        | string  | Link para a página popup                                      |
| <span style="color: #4169E1;">field</span>        | string  | Name do campo no formulário                                   |
| <span style="color: #4169E1;">variableName</span> | string  | Usa o nome da variável ao invés do name do campo              |
| <span style="color: #4169E1;">fixedValue</span>   | boolean | **VERIFICAR**                                                 |

###SetPopUpWidth

Define a largura, em pixels, do popup.

| Parâmetros                                        | Tipo    | Descrição                                                     |
| ------------------------------------------------- | ------- | ------------------------------------------------------------- |
| <span style="color: #4169E1;">width</span>        | int     | Largura do popup        

###SetPopUpHeight

Define a altura, em pixels, do popup.

| Parâmetros                                        | Tipo    | Descrição                                                     |
| ------------------------------------------------- | ------- | ------------------------------------------------------------- |
| <span style="color: #4169E1;">height</span>       | int     | Altura do popup                                               |

###SetPopUpHeight

Define a altura, em pixels, do popup.

| Parâmetros                                        | Tipo    | Descrição                                                     |
| ------------------------------------------------- | ------- | ------------------------------------------------------------- |
| <span style="color: #4169E1;">height</span>       | int     | Altura do popup                                               |

###SetTip

Adicion um objeto Tip junto ao objeto em instânciado.

É possível também colocar um link no Tip que resultará na abertura um popup.

**Tip** é um objeto que possui uma imagem chamativa, utilizado para detalhar o funcionamento de um processo ou campo.

**OBS**: Tip só pode ser utilizado em conjunto com o objeto [JForm](../../jaguar/JForm).

| Parâmetros                                        | Tipo    | Descrição                                                     |
| ------------------------------------------------- | ------- | ------------------------------------------------------------- |
| <span style="color: #4169E1;">title</span>        | string  | Define o título do Tip                                        |
| <span style="color: #4169E1;">tip</span>          | string  | Descrição do Tip                                              |
| <span style="color: #4169E1;">url</span>          | string  | Link para o popup                                             |
| <span style="color: #4169E1;">width</span>        | int     | Largura do popup                                              |
| <span style="color: #4169E1;">height</span>       | int     | Altura do popup                                               |
| <span style="color: #4169E1;">twidth</span>       | string  | Largura do Tip                                                |
| <span style="color: #4169E1;">theight</span>      | string  | Altura do Tip                                                 |

###SetError

Define uma mensagem de erro.

Pode ser utilizado com o método [SetCondition](../../jaguar/JFormNumber)

| Parâmetros                                     | Tipo    | Descrição                            |
| ---------------------------------------------- | ------- | ------------------------------------ |
| <span style="color: #4169E1;">error</span>     | string  | Mensagem de erro                     |

###SetTooltip

Adiciona uma mensagem de auxilio sobre o objeto, quando o ponteiro do mouse passa por cima dele.

| Parâmetros                                     | Tipo    | Descrição                            |
| ---------------------------------------------- | ------- | ------------------------------------ |
| <span style="color: #4169E1;">text</span>      | string  | Mensagem de auxilio                  |
