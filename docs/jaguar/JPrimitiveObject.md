#JPrimitiveObject

O JPrimitiveObject é a base para a maioria, se não todos, os objetos Jaguar. Todos os métodos e atributos declarados nessa classe são acessíveis nas demais classes derivadas.

Basicamente o JPrimitiveObject, através do chamado de métodos e definições de atributos, constrói um bloco de html, que pode ser exibido como uma página pré-moldada ao usuário.

##Principais Atributos

| Atributo   | Funcionamento                                                        |
| ---------- | -------------------------------------------------------------------- |
| $mType     | Armazena o tipo do objeto                                            |
| $mName     | Armazena o nome do objeto                                            |
| $mOnLoadJS | Armazena o JS que irá rodar quando a página estiver carregada        |
| $mHTML     | Armazena o bloco HTML que irá gerar                                  |
| $mJS       | Armazena os blocos de JS																						  |
| $mJSFile   | Armazena os arquivos externos de JS que será utilizado na página     |
| $mCSS      | Armazena os blocos de CSS                                            |
| $mCSSFile  | Armazena os arquivos externos de CSS que será utilizado na página    |
| $mObjects  | Armazena outros objetos dentro de um objeto                          |

##Principais Métodos

###MakeId

Define e retorna a tag **id** do objeto

| Parâmetros                               | Tipo | Descrição           |
| ---------------------------------------- | ---- | ------------------- |
| <span style="color: #4169E1;"> id</span> | int  | Define o valor do id|

###MakeClass

Define e retorna o nome de uma classe CSS do objeto

| Parâmetros                                 | Tipo    | Descrição              |
| ------------------------------------------ | ------- | ---------------------- |
| <span style="color: #4169E1;">class</span> | string  | Define o nome da classe|

###AddHTML

Adiciona um bloco HTML na página

| Parâmetros                                    | Tipo    | Descrição                                                                      |
| --------------------------------------------- | ------- | ------------------------------------------------------------------------------ |
| <span style="color: #4169E1;">what</span>     | string  | O bloco HTML a ser inserido                                                    |
| <span style="color: #4169E1;">position</span> | string  | A posição, em relação ao objeto, em que o bloco será inserido (antes ou depois)|

###AddJS

Adiciona um bloco JS na página

| Parâmetros                                    | Tipo    | Descrição                                                                      |
| --------------------------------------------- | ------- | ------------------------------------------------------------------------------ |
| <span style="color: #4169E1;">what</span>     | string  | O bloco JS a ser inserido                                                      |
| <span style="color: #4169E1;">position</span> | string  | A posição, em relação ao objeto, em que o bloco será inserido (antes ou depois)|

###AddJSFile

Adiciona um arquivo externo JS na página

| Parâmetros                                    | Tipo    | Descrição                                                                           |
| --------------------------------------------- | ------- | ----------------------------------------------------------------------------------- |
| <span style="color: #4169E1;">what</span>     | string  | Nome do arquivo JS a ser inserido                                                   |
| <span style="color: #4169E1;">position</span> | string  | A posição, em relação ao documento, em que o arquivo será inserido (antes ou depois)|

###AddCSS

Adiciona um bloco CSS na página

| Parâmetros                                    | Tipo    | Descrição                                                                      |
| --------------------------------------------- | ------- | ------------------------------------------------------------------------------ |
| <span style="color: #4169E1;">what</span>     | string  | O bloco CSS a ser inserido                                                     |

###AddCSSFile

Adiciona um arquivo externo CSS na página

| Parâmetros                                    | Tipo    | Descrição                                                                           |
| --------------------------------------------- | ------- | ----------------------------------------------------------------------------------- |
| <span style="color: #4169E1;">what</span>     | string  | Nome do arquivo JS a ser inserido                                                   |

###AddObject

Adiciona a referência de um objeto ao array de objetos da classe e incrementa o índice principal.

| Parâmetros                                    | Tipo             | Descrição                                                                      |
| --------------------------------------------- | ---------------- | ------------------------------------------------------------------------------ |
| <span style="color: #4169E1;">what</span>     | Object / string  | Adiciona um objeto no array de objetos e incrementa o índice principal         |

###GetHtml

Retorna, em formato de **String** o html do objeto

| Parâmetros | Tipo             | Descrição           |
| :--------: | :--------------: | :-----------------: |
| -          | -                | -                   |