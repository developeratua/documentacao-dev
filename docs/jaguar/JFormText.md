#JFormText

Cria um campo de formulário do tipo texto.
Extende a classe [JFormObject](../../jaguar/JFormObject).

```html
<input class="f_nm_objeto" id="1" type="text" name="f_nm_objeto" value="" size="40"/>
```

##Principais Atributos

| Atributo           | Funcionamento                                                                                         |
| ------------------ | ----------------------------------------------------------------------------------------------------- |
| $mUpper            | Controla se o valor do objeto deve ser retornado em letras maiúsculas                                 |
| $mLower            | Controla se o valor do objeto deve ser retornado em letras minúsculas                                 |
| $mTrim             | Controla se o valor do objeto deve ser aparado antes de retornar                                      |
| $mSpecialCharacter | Controla se o valor do objeto deve ter charecters especiais convertidos antes de retornar o seu valor |

##Principais Métodos

###JFormText

Construtor da classe

| Parâmetros                                | Tipo    | Descrição                            |
| ----------------------------------------- | ------- | ------------------------------------ |
| <span style="color: #4169E1;">name</span> | string  | Define o nome do objeto              |
| <span style="color: #4169E1;">value</span>| string  | Define um valor inicial para o objeto|

###SetDisabled

Define o objeto como inativo

| Parâmetros                                    | Tipo    | Descrição                         |
| --------------------------------------------- | ------- | --------------------------------- |
| <span style="color: #4169E1;">disabled</span> | boolean | Define se objeto está ativo ou não|

###SetUpper

Define que o texto inserido no campo ficará todo em maiúsculo

| Parâmetros                                    | Tipo    | Descrição                                                                      |
| --------------------------------------------- | ------- | ------------------------------------------------------------------------------ |
| <span style="color: #4169E1;">upper</span>    | boolean | Define se campo ficará em maiúsculo ou não                                     |

###SetLower

Define que o texto inserido no campo ficará todo em minúsculo

| Parâmetros                                    | Tipo    | Descrição                                                                      |
| --------------------------------------------- | ------- | ------------------------------------------------------------------------------ |
| <span style="color: #4169E1;">lower</span>    | boolean | Define se campo ficará em minúsculo ou não                                     |

###SetSpecialCharacter

Define se os caracteres especiais podem ser convertidos em caracteres comuns ou não

| Parâmetros                                               | Tipo    | Descrição                                                                      |
| -------------------------------------------------------- | ------- | ------------------------------------------------------------------------------ |
| <span style="color: #4169E1;">specialCharacter</span>    | boolean | Define se os caractéres serão convertidos ou não                               |

###SetTrim

Define se o valor do objeto deve ser cortado antes de retornar seu valor e como

| Parâmetros                                    | Tipo    | Descrição                                                                           |
| --------------------------------------------- | ------- | ----------------------------------------------------------------------------------- |
| <span style="color: #4169E1;">trim</span>     | string  | Define a posição do trim <span style="color: #2E8B57;">[left - all - right]</span>  |