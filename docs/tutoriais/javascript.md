#Javascript

Hoje, por padrão, utilizamos preferencialmente JQuery ao invés de Javascript puro, as vantagens são inúmeras facilitando muito a vida de quem programa.

Para documentação completa do JQuery acesse esse [link](http://api.jquery.com/).

Para algumas especificações de Javascript acesse esse [link](https://www.w3schools.com/js/).

---

##Campos de exemplo

Abaixo a criação de campos de exemplo para os próximos comandos

```php
<?php
  //type: text - JFormText
  $text = new JFormText("f_text_name");

  //type: select - JFormSelect
  $select = new JFormSelect("f_select_name");
  $select->AddOption(2, "Dois");
  $select->AddOption(3, "Três");
  $select->AddOption(4, "Quatro");
  
  //type: checkbox - JFormCheckBox
  $checkbox = new JFormCheckBox("f_checkbox_name");
  
  //type: button - JFormButton
  $button = new JFormButton("f_button_name");
```

##Métodos do JForm

```js
  //Função que será chamada
  function addFunction() {
    console.log('Você submeteu o formulário!');
  }
```

```php
<?php
  //Função javascript que será chamada quando o formulário for submetido
  $form->AddFunction("addFunction");
```

##Métodos dos objetos do JForm

Métodos utilizados nos objetos do JForm ([JFormText](../../jaguar/JFormText), [JFormSelect](../../jaguar/JFormSelect), [JFormCheckbox](../../jaguar/JFormCheckBox), [JFormButton](../../JFormButton), etc ...).

###MakeClass

Por padrão, o jaguar coloca a class igual ao "name" que foi passado para o objeto, com esse método é possível criar uma class diferente

```php
<?php
  $text->MakeClass("textClass");
  $select->MakeClass("selectClass");
  $checkbox->MakeClass("checkboxClass");
  $button->MakeClass("buttonClass");
```

###MakeId

Por padrão, o jaguar cria um id aleatório para o campo, com esse método é possível criar um id diferente

```php
<?php
  $text->MakeId("textId");
  $select->MakeId("selectId");
  $checkbox->MakeId("checkboxId");
  $button->MakeId("buttonId");
```

##Seletores mais utilizados

###Buscando pelo 'name'

```js
  //Javascript
  document.getElementsByName('f_text_name');
  document.getElementsByName('f_select_name');
  document.getElementsByName('f_checkbox_name');
  
  //JQuery
  $('input[name=f_text_name]');
  $('select[name=f_select_name]');
  $('checkbox[name=f_checkbox_name]');
```

####Buscando pela 'class'

```js
  //Javascript
  document.getElementsByClassName('textClass');
  document.getElementsByClassName('selectClass');
  document.getElementsByClassName('checkboxClass');
  
  //JQuery
  $('.textClass');
  $('.selectClass');
  $('.checkboxClass');
```

###Buscando pelo 'id'

```js
  //Javascript
  document.getElementById('textId');
  document.getElementById('selectId');
  document.getElementById('checkboxId');
  
  //JQuery
  $('#textId');
  $('#selectId');
  $('#checkboxId');
```

Para demais seletores jquery acesse esse [link](https://api.jquery.com/category/selectors/)

##Funções javascript

###Closures

Para mais informações, acesse o [link](https://developer.mozilla.org/pt-BR/docs/Web/JavaScript/Guide/Closures)

```js
  //A variável count só existe dentro do escopo do "add"
  var add = (function () {
    var count = 0;
    return function () {
      return count += 1;
    }
  })();
  
  console.log(add()); //1
  console.log(add()); //2
  console.log(add()); //3
  
  //Como citado anteriormente, esta variável não existe fora do escopo interno do "add"
  console.log(count); //Uncaught ReferenceError: count is not defined
```

###Pop

```js
  //Função utilizada no arquivo pop_pessoa.php
  function pop_up_back(cdPessoa, nmPessoa)
  {
    top.opener.pop_up_back_pessoa(cdPessoa, nmPessoa);
    window.close();
  }
```

```php
<?php
  //Função utilizada no arquivo onde vai ser utilizado o campo Pop
  function pop_up_back_pessoa(cdPessoa, nmPessoa)
  {
    $('input[name=f_cd_pessoa]').val(cdPessoa);
    $('input[name=f_nm_pessoa]').val(nmPessoa);
  }
```

```php
<?php
  //Exemplo mais comum do uso do Pop
  $cd_pessoa = new JFormNumber("f_cd_pessoa");
  $cd_pessoa>AddPopUpLabel("Consultar");
  $cd_pessoa>AddPopUpUrl("pop_pessoa.php");
  
  $nm_pessoa = new JFormText("f_nm_pessoa");
```

##Funções jQuery UI

###.dialog()

Para mais informações, acesse o [link](http://api.jqueryui.com/dialog/)

```php
<?php
  $html->AddHtml("<div id='confirm' style='display: none'>Qual valor você deseja atribuir para o campo?</div>");
```

```js
  $('#confirm').dialog({
    resizable: false,
    height: 'auto',
    width: 400,
    modal: true,
    buttons: {
      'Botão 1': function() {
        $('.campo_class').val('Valor 1');
        $(this).dialog('close');
      },
      'Botão 2': function() {
        $('.campo_class').val('Valor 2');
        $(this).dialog('close');
      }
    }
  });
```

##Funções (jaguar.utils.js)

###adiciona_campos

Adiciona linhas com campos de input em um formulário.

| Parâmetro        | Tipo    | Valor Padrão    | Descrição                                                                                                                             |
| ---------------- | :-----: | :-------------: | ------------------------------------------------------------------------------------------------------------------------------------- |
| nameCampoInicial | string  |                 | Nome do campo que será utilizado como base para criar os demais campos                        																				 |
| linhaBase        | string  |                 | Id atribuído para a linha base dos campos ([OpenRow](../../jaguar/JTable#openrow))            																				 |
| tablePrincipal   | string  |                 | 																																															 																				 |
| quantidadeMax    | int     |                 | Quantidade máxima de campos que poderão ser criados																					 																				 |
| botaoAdd         | string  |                 | Elemento que chama a função. É utilizado para remover o elemento da tela quando atingir a quantidade máxima de campos (quantidadeMax) |
| contador         | string  |                 | Campo do tipo hidden que irá guardar o número de campos que foram criados                                                             |
| header           | string  |                 | Id atribuído para o header do campo ([OpenHeader](../../jaguar/JTable#openheader))                                                    |
| ultimaLinha      | string  |                 | Id atribuído para a linha final dos campos ([OpenRow](../../jaguar/JTable#openrow))                                                   |
| qtInicial        | int     |                 |                                                                                                                                       |
| frame            | string  |                 |                                                                                                                                       |
| id               | string  |                 |                                                                                                                                       | 


Exemplo de utilização da função adiciona_campos:

```js
//Javascript
$(document).ready(function() {
  $('#botaoAdd').click(function() {
    adiciona_campos(
      'f_nm_campo_',  // nameCampoInicial
      'linhaCampo',   // linhaBase
      'JTable',       // tablePrincipal
      10,             // quantidadeMax
      'botaoAdd',     // botaoAdd
      'f_qt_linhas',  // contador
      'headerCampo_', // header
      'ultimaLinha'   // ultimaLinha
                      // qtInicial
                      // frame
                      // id
    );
  });
});
```

```php
<?php
  //PHP
  $form->OpenRow();
  $form->OpenHeader("Campo <a href=\"#\" id=\"botaoAdd\">(+)</a>");
  
  for ($i = 1; $i <= 1; $i++)
  {
    $form->OpenRow(array("id" => "linhaCampo"));
    $form->OpenHeader("Campo {$i}", array("id" => "headerCampo_1"));
    $form->OpenCell();
    $nm_campo = "nm_campo_$i";
    $$nm_campo = new JFormText("f_{$nm_campo}");
    $form->AddObject($$nm_campo);
  }
  
  $qt_linhas = new JFormHidden("f_qt_linhas");
  $form->AddObject($qt_linhas);
  
  $form->OpenRow(array("id" => "ultimaLinha"));
```

###buscaDadosAjax

Realiza uma requisição AJAX para um método de uma classe no servidor e retorna um objeto ou um array de objetos json.

| Parâmetro        | Tipo    | Valor Padrão    | Descrição                  |
| ---------------- | :-----: | :-------------: | -------------------------- |
| classe 	         | string  |                 | Nome do classe             |
| acao             | string  |                 | Método que vai ser chamado |
| param            | json    | {}              | Parâmetros do método       |
| isPost           | string  | "GET"           | Se é POST OU GET           |
| isAsync          | boolean | false           | Se é assíncrono            |

```js
$(document).ready(function() {
  var buscaDadosAjax = buscaDadosAjax('Pessoa', 'obterDadosPessoa', {cdPessoa: 1});
  console.log(buscaDadosAjax);
});
```
Obs: o método chamado na classe deve possuir a anotação uses no PHPDoc. Exemplo:

```php
<?php
  class Pessoa 
  {
    /**
     * @uses ajaxComplete
     * @param int $cdPessoa
     */
    public static function obterDadosPessoa($cdPessoa)
    {
      ...
    }
  }
```

###dadosAjax

Monta um JSON para ser utilizado em conjunto com a função [buscaDadosAjaxAsync](../../tutoriais/javascript#buscaDadosAjaxAsync).

| Parâmetro        | Tipo    | Valor Padrão    | Descrição                  |
| ---------------- | :-----: | :-------------: | -------------------------- |
| classe 	         | string  |                 | Nome do classe             |
| acao             | string  |                 | Método que vai ser chamado |
| param            | json    | {}              | Parâmetros do método       |


```js
var dadosAjax = dadosAjax('Pessoa', 'obterDadosPessoa', {cdPessoa: 1});
```

###buscaDadosAjaxAsync

Realiza uma requisição AJAX para um método de uma classe no servidor e retorna um objeto ou um array de objetos json, de forma assíncrona.

| Parâmetro        | Tipo     | Valor Padrão    | Descrição                         |
| ---------------- | :------: | :-------------: | --------------------------------- |
| data  	         | json     | {}              | Json com classe, método e filtros |
| acao             | boolean  |                 | Se é POST OU GET                  |

```js
$(document).ready(function() {
  var dadosAjax = dadosAjax('Pessoa', 'obterDadosPessoa', {cdPessoa: 1});
  buscaDadosAjaxAsync(dadosAjax).then(function(retorno) {
    console.log(retorno);
  });
});
```

Obs: o método chamado na classe deve possuir a anotação uses no PHPDoc.

```php
<?php
  /**
   * @uses ajaxComplete
   * @param int $cdPessoa
   */
  public static function obterDadosPessoa($cdPessoa)
  {
    ...
  }
```

###abre_pop

Adiciona um popup na tela.

Essa função encontra-se [funcoes.inc.php](../../jaguar/funcoes).

É usada em conjunto com o PHP.

| Parâmetro        | Tipo     | Valor Padrão    | Descrição                          |
| ---------------- | :------: | :-------------: | ---------------------------------- |
| endereco  	     | string   |                 | Json com classe, método e filtros  |
| hideclose        | boolean  |                 | Se é POST OU GET                   |
| onClose          | callback |                 | Callback que roda no fechar do pop |


```php
<?php
  $html  = new JHtml($title);

  //nesse momento todo o html do pop será adicionado na página
  $html->AddHtml(abre_pop());

  echo $html->GetHtml();
```

```js
abre_pop('teste.php?f_id=1', true, function(){ alert(‘fechou’); });
```

###pop_open

| Parâmetro        | Tipo     | Valor Padrão    | Descrição                                               |
| ---------------- | :------: | :-------------: | ------------------------------------------------------- |
| address  	       | string   |                 | URL que do arquivo que será aberto dentro do pop        |
| width            | int      |                 | Largura do pop                                          |
| height           | int      |                 | Altura do pop                                           |
| windowName       | string / boolean |         | Nome da janela do pop                                   |
| resizable        | string   |                 | Define se irá permitir que a janela seja redimensionada |

```js
<a href=\"javascript:void(0);\" onClick=\"javascript:pop_open('pop_teste.php?f_id=1', 1000, 600, false, 'yes');});\">Link do Pop</a>
```

###formatNum

Campo de exemplo para se demonstrar como funciona as funções de conversão de dados.

```php
<?php
  $campo = new JFormFormattedNumber("f_campo_name");
  $campo->SetDefaultValue("2568.41");
  $campo->MakeClass("campoClass");
```

Nesse exemplo, o campo é uma String com valor de: 2.568,41

```js
var campo = $('.campoClass').val();
console.log(campo);

```

A função irá transformar o campo para Number, utilizando duas casas após o ponto. Agora o valor do campo é: 2568.41.

```js
var campoNumber = formatNum(campo, 2, 'sys');
console.log(campoNumber);
```

A função irá transformar o campo para String, utilizando duas casas após o vírgula. Agora o valor do campo é: 2.568,41.

```js
var campoString = formatNum(campoNumber, 2, 'pt_BR');
console.log(campoString);
```

##Funções para conversão de dados (string / number)

Campo de exemplo

```php
<?php
 $campo = new JFormFormattedNumber("f_campo_name");
 $campo->SetDefaultValue("2568.41");
 $campo->MakeClass("campoClass");
```

Valor do campo no padrão pt_BR (type = string)

```js
var campo1 = $('input.campoClass').val();
console.log(campo1);
```

Valor do campo no padrão sys (type = string)

```js
var campo2 = transform_value(campo1);
console.log(campo2);
```

Valor do campo no padrão sys (type = number)

```js
var campo3 = parseFloat(campo2);
console.log(campo3);
```

Valor do campo no padrão sys não arredondado (type = number)

```js
var campo4 = campo3 + 5555.654321321231;
console.log(campo4);
```
Valor do campo no padrão sys arredondado (type = number)

```js
var campo5 = Math.round(campo4 * Math.pow(10 , 2)) / Math.pow(10 , 2);
console.log(campo5);
```

Valor do campo no padrão pt_BR arredondado (type = string)
```js
var campo6 = return_formatted_value(campo5);
console.log(campo6);
```
