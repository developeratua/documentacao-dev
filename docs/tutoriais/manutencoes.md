#Manutenções

[comment]: <> (Ajustar a descrição)

O objetivo desse tutorial é demonstrar o desenvolvimento áreas do sistema responsáveis por gerenciar o conteúdo de áreas específicas.

---

##JMaintenance

O [JMaintenance](../../jaguar/JMaintenance) é a classe responsável pelo gerenciamento dos dados de uma tabela através de uma interface. Com ela é possível desenvolver a parte de **CREATE**, **UPDATE** e **DELETE** do CRUD de uma tabela. A parte do **READ** é de responsabilidade do [JDBGrid](../../jaguar/JDBGrid).

No momento em que o [JMaintenance](../../jaguar/JMaintenance) é instanciado, são passados dois parâmetros para ele, sendo o primeiro uma instância de conexão com o banco de dados, [JDBConn](../../jaguar/JDBConn), e o segundo o título da manutenção.

O segundo passo é definir a tabela e a classe do item a ser manipulado. Isso é feito através do método [SetDBTable](../../jaguar/JMaintenance#setdbtable).

É possível definir o redirecionamento de página após uma ação utilizando o método [SetLocation](../../jaguar/JMaintenance#setlocation), onde primeiro determina-se a ação, **insert**, **update** ou **delete**, e em seguida o ponto de redirecionamento. Deve-se levar em consideração que, em páginas que necessitam receber dados para seu preenchimento, estes devem ser enviados por via de um array no terceiro parâmetro do método.

As colunas da tabela do banco de dados são representadas pelos objetos Jaguar, e adicionadas na manutenção através do método [AddDBField](../../jaguar/JMaintenance#adddbfield).

Quando todos os campos estiverem definidos, deve-se realizar a "construção" da manutenção, essa, por sua vez, realizada pelo método [BuildEndMaintenance](../../jaguar/JMaintenance#buildendmaintenance).

```php
<?php
  require_once("lib/jaguar/jaguar.inc.php");
  require_once("include/funcoes.inc.php");
  require_once("md_pessoa.php");

  $ManBD = ManBD::getInstance();
  $ManBD->objConn->SetDebug(0, 1);

  JDBAuth::SetValidated();

  $man = new JMaintenance($ManBD->objConn, "Manutenção de Pessoa");
  $man->SetDBTable("pessoa", "Pessoa");
  $man->AddMasterDetail($master);

  $key = ["f_cd_pessoa" => $_REQUEST["f_cd_pessoa"]];
  $man->SetLocation("insert", "man_pessoa_endereco.php", $key);
  $man->SetLocation("update", "man_pessoa_endereco.php", $key);
  $man->SetLocation("delete", "sel_pessoa.php");

  $cd_pessoa = new JFormHidden("f_cd_pessoa");
  $man->AddDBField("cd_pessoa", $cd_pessoa, false, true);

  $nm_pessoa = new JFormText("f_nm_pessoa");
  $nm_pessoa->SetTestIfEmpty(true, "Preenca o campo Pessoa!");
  $man->AddDBField("nm_pessoa", $nm_pessoa, "Pessoa");

  $cd_cidade = new JFormSelect("f_cd_cidade");
  $cd_cidade->SetTestIfEmpty(true, "Preencha o campo Cidade!");
  $cd_cidade->SetFirstEmpty(true);
  $cd_cidade->AddOption(1, "Cidade 1");
  $cd_cidade->AddOption(2, "Cidade 2");
  $man->AddDBField("cd_cidade", $cd_cidade, "Cidade");

  $ds_apelido = new JFormText("f_ds_apelido");
  $man->AddDBField("ds_apelido", $ds_apelido, "Apelido");

  $ds_senha = new JFormPassword("f_ds_senha");
  $man->AddDBField("ds_senha", $ds_senha, "Senha");

  $ds_email = new JFormEmail("f_ds_email");
  $man->AddDBField("ds_email", $ds_email, "E-mail");

  $ds_observacao = new JFormTextArea("f_ds_observacao");
  $ds_observacao->SetSize(40, 5);
  $man->AddDBField("ds_observacao", $ds_observacao, "Observação");

  $man->BuildEndMaintenance();
  $man->AddHtml("<br/><a href=\"sel_pessoa.php\">Listagem de Pessoas</a> | <a href=\"con_pessoa.php?f_cd_pessoa={$_REQUEST['f_cd_pessoa']}\">Extrato de Pessoa</a>");

  echo $man->GetHtml();
```

![alt text](../../img/manutencao_01.png "Manutenção")

##JMasterDetail

O [JMasterDetail](../../jaguar/JMasterDetail) permite a criação de abas dentro de uma manuteção, essas abas geralmente são consequências de relações **many-to-one** da tabela principal, ex: pessoa (tabela princial) e pessoa_telefone (aba disposta no [JMasterDetail](../../jaguar/JMasterDetail) relacionada a partir de pessoa).

A aplicação do [JMasterDetail](../../jaguar/JMasterDetail) em uma manutenção é bem simples, deve-se declara-lo em um arquivo a parte, utilizando o prefixo **md_**, e incluí-lo no arquivo da manutenção através da função **require_once** e adicionar o objeto [JMasterDetail](../../jaguar/JMasterDetail) no [JMaintenance](../../jaguar/JMaintenance) através do método [AddMasterDetail](../../jaguar/JMaintenance#addmasterdetail). Vide o exemplo anterior.

A criação de um [JMasterDetail](../../jaguar/JMasterDetail) é feita em 3 passos básicos:

1. Instanciar o [JMasterDetail](../../jaguar/JMasterDetail);
2. No método [SetDefaultFile]() defini-se a manutenção principal;
3. Cria-se um array contendo todos os links, os títulos e as chaves das manutenções derivadas da tabela principal e adiciona-se através do método [AddMasterDetail]().



```php
<?php
  $master = new JMasterDetail();
  $master->SetDefaultFile("man_pessoa.php");

  $key = ["f_cd_pessoa"];

  $parameters[] = ["man_pessoa.php",          "Pessoa",   $key];
  $parameters[] = ["man_pessoa_endereco.php", "Endereço", $key];
  $parameters[] = ["man_pessoa_telefone.php", "Telefone", $key];

  $master->AddMasterDetail($parameters);
```



