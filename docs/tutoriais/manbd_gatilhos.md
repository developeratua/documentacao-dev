#ManBD e Gatilhos

Esse tutorial irá abordar o funcionamento básico do [ManBD](../../jaguar/ManBD) e o uso de gatilhos nas classes.

##ManBD

O [ManBD](../../jaguar/ManBD) é um classe de persistência, alteração, exclusão e pesquisa a partir de objetos mapeados conforme o banco de dados.

###Incianciando objetos

Quando precisamos de uma instância do objeto [ManBD](../../jaguar/ManBD), o ideial é sempre obter uma instância já existente ao invés de criar uma nova, isso ajuda a seguir o padrão sigleton.

```php
<?php
  $ManBD = ManBD::getInstance();
  $ManBD->objconn->SetDebug(0, 1);
```

Para instanciar um objeto, no padrão Jaguar, se faz necessário passar a referência de uma conexão no banco.

```php
<?php
  $ManBD = ManBD::getInstance();
  $ManBD->objconn->SetDebug(0, 1);
  
  $ClasseTabela = new ClasseTabela($ManBD->objConn);
```

###Populando um objeto com o ManBD

Utilizando o Jaguar, e as classes no seu padrão, é possível obter todos os dados de um objeto, ou até mesmo um array de objetos, a partir das características definidas no objeto.

Por exemplo, se você instânciar um objeto **Cidade** e definir seu nome, o [ManBD](../../jaguar/ManBD), ao popular o objeto, vai trazer todos os dados da cidade. Agora, se no objeto cidade for definido o estado, o [ManBD](../../jaguar/ManBD) vai retornar um array de cidades as quais fazem parte daquele estado.

```php
<?php
  $ManBD = ManBD::getInstance();
  $ManBD->objconn->SetDebug(0, 1);
  
  /**
   * Dessa forma, preenche todo o objeto Cidade levando, em consideração
   * que só exista uma cidade chamada Passo Fundo
   */
  $Cidade = new Cidade($ManBD->objConn);
  $Cidade->nm_cidade = "Passo Fundo";
  $ManBD->PopulaObjetoGenerico($Cidade);

  /**
   * Dessa forma, o ManBD retorna um array de cidades que
   * possuam o estado Rio Grande do Sul
   */
  $Cidade = new Cidade($ManBD->objConn);
  $Cidade->nm_estado = "Rio Grande do Sul";
  $arrCidades = $ManBD->PopulaObjetoGenerico($Cidade);
```

Agora, digamos você queira todas as cidades do Rio Grande do Sul que possuam a mais de 3.000 habitantes. Isso não é possível de fazer **diretamente na classe**, porém, o [ManBD](../../jaguar/ManBD) possui um **parâmetro** para que seja possível passar uma restrição **WHERE** no momento de popular o objeto.

```php
<?php
  $ManBD = ManBD::getInstance();
  $ManBD->objconn->SetDebug(0, 1);

  $Cidade = new Cidade($ManBD->objConn);
  $Cidade->nm_estado = "Rio Grande do Sul";
  $arrCidades = $ManBD->PopulaObjetoGenerico($Cidade, false, "AND qt_habitantes > 3000");
```

###Persistindo um objeto

O [ManBD](../../jaguar/ManBD) não possui métodos distintos para inserir e alterar, ao invés disso, ele contém o método salvar, que verifica se as chaves primárias já existem, caso não existam realiza o insert, caso o contrário, realiza a atualização do objeto.

```php
<?php
  $ManBD = ManBD::getInstance();
  $ManBD->objconn->SetDebug(0, 1);

  //Persiste uma NOVA Cidade
  $Cidade = new Cidade($ManBD->objConn);
  $Cidade->nm_cidade     = "Marau";
  $Cidade->qt_habitantes = 40629;
  $Cidade->nr_cep        = 99150000;
  $ManBD->Salvar($Cidade);

  /**
   * Sobreescre o nome da cidade onde, nos registros,
   * a chave primária possuir o valor 123
   */
  $Cidade = new Cidade($ManBD->objConn);
  $Cidade->cd_cidade = 123;
  $Cidade->nm_cidade = "Carazinho";
  $ManBD->Salvar($Cidade);
```

Caso seja definido uma chave primária inexistente, ocorrerá erro de sql.

##Gatilhos

Gatilhos são funções pré-definidas nas classes, no padrão Jaguar, que serão executadas antes ou depois das ações de inserir, alterar ou excluir. Esses gatilhos rodam sempre, ao realizar uma das operações descritas anteriormente, tanto nas manutenções quanto realizando aplicando-as diretamente em uma classe, por exemplo, chamando o método [salvar](../../jaguar/ManBD#salvar) do [ManBD](../../jaguar/ManBD).

Os gatilhos podem ser declarados de duas formas dentro de uma classe. Em qualquer uma das formas é necessário primeiro criar um método público.

A primeira forma, e a utilizada por padrão, é pelo nome do método, são eles "**antes_inserir**", "**antes_alterar**", "**antes_excluir**", "**depois_inserir**", "**depois_alterar**" e "**depois_excluir**". Obrigatoriamente esses  métodos devem receber como parâmetro uma referência do [ManBD](../../jaguar/ManBD).

A segunda forma é através de **annotations** nos métodos, são eles "**@trBeforeInsert**", "**@trBeforeUpdate**", "**@trBeforeDelete**", "**@trAfterInsert**", "**@trAfterUpdate**" e "**@trAfterDelete**". 

Vale ressaltar que o segundo método só deve ser utilizado em casos específicos, pois eles **irão rodar mesmo que, no [ManBD](../../jaguar/ManBD), seja desativada a função de rodar gatilhos**. Utilizando-se dessa forma, também é possível definir que o método será chamado em várias circunstâncias, como por exemplo, antes de inserir e depois de atualizar.

```php
<?php
  class Tabela
  {
    public $key;

    public $table;

    public $objConn;

    public function __construct(&$objConn)
    {
      $this->key     = array("cd_tabela");
      $this->table   = "tabela";
      $this->objConn = $objConn;
    }

    /**
     * @param ManBD $ManBD
     */
    public function antes_inserir(ManBD &$ManBD)
    {
      echo "Roda antes de inserir o registro";
    }

    /**
     * @param ManBD $ManBD
     */
    public function antes_atualizar(ManBD &$ManBD)
    {
      echo "Roda antes de atualizar o registro";
    }

    /**
     * @param ManBD $ManBD
     */
    public function antes_excluir(ManBD &$ManBD)
    {
      echo "Roda antes de excluir o registro";
    }

    /**
     * @param ManBD $ManBD
     */
    public function depois_inserir(ManBD &$ManBD)
    {
      echo "Roda depois de inserir o registro";
    }

    /**
     * @param ManBD $ManBD
     */
    public function depois_atualizar(ManBD &$ManBD)
    {
      echo "Roda depois de altualizar o registro";
    }

    /**
     * @param ManBD $ManBD
     */
    public function depois_excluir(ManBD &$ManBD)
    {
      echo "Roda depois de excluir o registro";
    }
	 
    /**
     * @trBeforeInsert
     * @trBeforeUpdate
     * @param ManBD $ManBD
     */
    public function antesInserirAlterar(ManBD &$ManBD)
    {
      echo "Esse método irá roda SEMPRE depois de inserir e/ou atualizar";
    }
  }

```