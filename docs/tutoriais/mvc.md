#MVC

Nesses tutorial será mostrado o desenvolvimento de um processo MVC utilizando as premissas do Jaguar.

---

##View

No padrão Jaguar, a parte da **View**, aquela que exibe a interface para o usuário, ocorre dentro de apenas um arquivo, com prefixo **exe_**, e as telas que fazem parte do processo são dividas em blocos.

###Classe View

A "classe **View**" na realizade é um **stdClass**, ou seja, uma classe vazia que é moldada a partir da sua utilização, que no View do padrão Jaguar de MVC, possui três finalidades básicas: controle de telas, distribuição de dados e controle de erros. O nome da variável que a armazena, por padrão deve ser **$View**.

####Controle de Telas

O controle de telas utilizando o **$View** é feito através da definição do atributo **idTela**. Esse atributo deve ser definido em cada ação do controller. A sua primeira definição deve acontecer no método [defaultAcao](../../tutoriais/mvc#defaultacao).

O destino de cada tela deve ser definido utilizando o método [SetAction](../../jaguar/JForm#setaction) do [JForm](../../jaguar/JForm). Ex:

```php
<?php
  $form->SetAction($_SERVER["PHP_SELF"] . "?acao=buscarPessoa");
```

####Distribuição de Dados

Com o **$View** é possível fazer a transição de dados do **view** até o **model** e vice-versa. Para isso basta apenas definir os valores dentro de um atributo da **$View** e acessar o mesmo no momento que for necessário.

####Controle de Erros

O controle de erros também ocorre dentro da **$View**, geralmente utilizando os atributos **aviso** e **erro** que devem ser tratados dentro do método [tratarErro](../../tutoriais/mvc#tratarErro) no **controller**. Geralmente os erros e avisos são exibidos na primeira tela do MVC.

Abaixo, um exemplo da **View** de um MVC no padrão Jaguar.

```php
<?php
  session_start();
  require_once("lib/jaguar/jaguar.inc.php");
  require_once("include/funcoes.inc.php");
  require_once("controle/VerificarPessoaControle.class.php");

  JDBAuth::SetValidated();

  $View  = new stdClass();
  $ManBD = new ManBD(new Erro(JDBConn::getInstance()), true, false);
  $ManBD->objConn->SetDebug(0, 1);

  new VerificarPessoaControle($ManBD, $View);

  $title = "Verificar Pessoa";
  $html  = new JHtml($title);
  $html->AddHtml("<h3>{$title}</h3>");

  $form = new JForm();

	//Tela incial, exibe os filtros para o processo
  if ($View->idTela == VerificarPessoaControle::TELA_FILTRO || str_value($View->aviso))
  {
    $form->SetAction($_SERVER["PHP_SELF"] . "?acao=buscarPessoa");

    $form->OpenRow();
    $form->OpenHeader("<b>Nome</b>");
    $form->OpenCell();
    $nm_pessoa = new JFormText("f_nm_pessoa");
    $nm_pessoa->SetTestIfEmpty(true, "Preencha o campo Nome!");
    $form->AddObject($nm_pessoa);

    if (str_value($View->aviso))
    {
      $form->OpenRow();
      $form->OpenHeader("<b>Status</b>");
      $form->OpenCell($View->aviso, ["colspan" => "3", "class" => "alert-danger text-danger"]);
    }

    fil_botao($form, 2, true, "Buscar");
  }
	//Tela que apresenta os dados que foram buscados anteriormente
  elseif ($View->idTela == VerificarPessoaControle::TELA_LISTAR)
  {
    $form->SetAction($_SERVER["PHP_SELF"] . "?acao=verificarPessoa");

    $form->OpenRow();
    $form->OpenHeader("Pessoa");
    $form->OpenCell();
    $cd_pessoa = new JFormSelect("f_cd_pessoa");
    $cd_pessoa->SetOptions($View->opIdPessoa);
    $form->AddObject($cd_pessoa);

    fil_botao($form, 2, false, "Verificar");
  }
  //Tela que exibe os resultados do processo
  elseif ($View->idTela == VerificarPessoaControle::TELA_RESULTADO || str_value($View->mensagem))
  {
    $form->OpenRow();
    $form->OpenHeader("Pessoa");
    $form->OpenHeader("Situação de cadastro");

    foreach ($View->arrVerificados as $Verificado)
    {
      $form->OpenRow();
      $form->OpenCell($Verificado["nm_pessoa"]);
      $form->OpenCell($Verificado["ds_atualizacao"]);
    }
  }

  $html->AddObject($form);

  if ($View->idTela != VerificarPessoaControle::TELA_FILTRO)
    $html->AddHtml("<br/><a href='exe_verificar_pessoa.php'>Voltar para o filtro</a>");

  echo $html->GetHtml();
```

##Controller

O **Controller** é utilizado para a distribuição de tarefas do processo, é nele que será descrito as rotas do processo.

Cada ação é direcionada a partir da **action** definida no formulário da **View**. Para ser acionada, a ação, representada por um método, deve possuir o mesmo nome da **action** do formulário, porém, com o sufixo "**Acao**". Dentro desse método é que será instanciado o **model** e através dos seus métodos, serão aplicadas as regras de negócio.

###defaultAcao

O principal método de redirecionamento no controller é o **defaultAcao**, é ele quem define qual será a primeira tela a ser exibida e as configurações iniciais. Esse método é obrigatório.

###tratarErro

O método **tratarErro** é chamado automaticamente, sempre que ocorrer uma exceção dentro do **model**. Nele são tratadas as mensagens de erro e sua classificação.

```php
<?php
  require_once("ControladorPadrao.class.php");

  class VerificarPessoaControle extends ControladorPadrao
  {
    protected $View;
    protected $ManBD;
    protected $objErro;
    private $objConn;

    const TELA_FILTRO    = "tela_filtro";
    const TELA_LISTAR    = "tela_listar";
    const TELA_RESULTADO = "tela_resultado";

    /**
     * @param ManBD $ManBD
     * @param stdClass $View
     */
    public function __construct(ManBD &$ManBD, stdClass &$View)
    {
      $this->objErro = $ManBD->objErro;
      $this->objConn = $ManBD->objErro->objConn;
      $this->ManBD   = $ManBD;
      $this->View    = $View;

      $this->execute();
    }

    protected function defaultAcao()
    {
      $this->View->idTela = self::TELA_FILTRO;
    }

    protected function buscarPessoaAcao()
    {
      $VerificarPessoa = new VerificarPessoa($this->ManBD, $this->View);
      $VerificarPessoa->buscar();

      $this->View->idTela = self::TELA_LISTAR;
    }

    protected function verificarPessoaAcao()
    {
      $this->View->idTela = self::TELA_RESULTADO;

      $VerificarPessoa = new VerificarPessoa($this->ManBD, $this->View);
      $VerificarPessoa->verificar();
    }

    /**
     * Realiza o tratamento de exceções.
     * As divide em erro e aviso.
     * 
     * @param Exception $e
     */
    protected function tratarErro(Exception $e)
    {
      $GLOBALS["trans_ok"] = false;

      if (get_class($e) == "Aviso")
        $this->View->aviso  = $e->getMessage();
      else
        $this->View->erro = "Ocorreram erros durante o processo de alteração.<br/><strong>Erro:</strong> {$e->getMessage()}";
    }
  }

```

##Model

O **model** é o local que deve comportar toda a lógica do processo, é nela que os dados serão manipulados e verificados, e registros serão inseridos, alterados e excluídos.

A única "regra" de utilização do **model**, é manter as mesmas instâncias definidas no **controller**. Isso é feito através da definição dos atributos. Vide o exemplo abaixo.

```php
<?php
  class VerificarPessoa
  {
    private $ManBD;
    private $objConn;
    private $objErro;
    private $View;

    /**
     * @param ManBD $ManBD
     * @param stdClass $View
     */
    public function __construct(ManBD &$ManBD, stdClass &$View)
    {
      $this->ManBD   = $ManBD;
      $this->objConn = $ManBD->objErro->objConn;
      $this->objErro = $ManBD->objErro;
      $this->View    = $View;
    }

    /**
     * Busca pessoas a partir do seu nome
     */
    public function buscar()
    {
      $sqlWhere  = "";
      $sqlWhere .= Relatorio::obterRestricaoWhere("pe.nm_pessoa", "~*", $_REQUEST["f_nm_pessoa"], "'");

      $sql = <<<SQL
        SELECT pe.nm_pessoa AS description,
               pe.cd_pessoa AS value
          FROM pessoa pe
         WHERE TRUE
           {$sqlWhere}
SQL;

      $this->View->opIdPessoa = $this->ManBD->select($sql);

      if (sizeof($this->View->opIdPessoa) == 0)
        throw new Aviso ("A pesquisa não retornou dados!");
    }

    /**
     * Verifica se a pessoa está atualizada ou não
     */
    public function verificar()
    {
      $sql = <<<SQL
        SELECT pe.nm_pessoa,
               CASE
                 WHEN pe.dt_atualizacao > CURRENT_DATE THEN 'DESATUALIZADO'
                 ELSE 'ATUALIZADO'
               END AS ds_atualizacao
          FROM pessoa pe
         WHERE pe.cd_pessoa = '{$_REQUEST["f_cd_pessoa"]}'
SQL;

      $this->View->arrVerificados = $this->ManBD->select($sql);
    }
  }
```