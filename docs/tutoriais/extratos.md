#Extratos

Nesse tutorial será demonstrado o uso do JConsultation para criação de extratos.

Os extratos são páginas de cunho informativo, nelas são exibidos todos os dados de uma determinada tabela e/ou conjunto de tabelas relacionadas.

---

##JConsultation

O [JConsultation](../../jaguar/JConsultation) é uma classe do Jaguar para a criação de tabelas informatívas. Essas tabelas podem ser dividias em várias áreas, onde geralmente, cada área representa uma tabela do banco de dados. Essas áreas são adicionadas utilizando o método [AddArea](../../jaguar/JConsultation#addarea) que, respectivamente, recebe o nome da área, o **SQL** da onde serão obtidos os dados e o título da área, esse pode ser um bloco html, como por exemplo um link.

As tabelas do [JConsultation](../../jaguar/JConsultation) são montadas a partir de um arrat de relação **nome da coluna => header**, estes são chamados **visibleFields** e atrelados ao objeto [JConsultation](../../jaguar/JConsultation) pelo método [SetVisibleFields](../../jaguar/JConsultation#setvisiblefields).

É possível estilizar as colunas a partir do método [SetCellOptons](../../jaguar/JConsultation#setcelloptions), onde pode-se definir coisas como, alinhamento, colspan, etc..

Para manter duas colunas na mesma linha utiliza-se o método [SetNoBreakLine](../../jaguar/JConsultation#setnobreakline) informando o nome da área e o campo.

Também é possível chamar uma função para manipular os valores de uma célula, para isso, deve-se chamar o método [SetCallback](../../jaguar/JConsultation#setcallback) que recebe  o nome da área, o índice do campo, o nome da função e os parâmetros necessários, sendo que o valor a ser formatado é o primeiro parâmetro a ser passado.


```php
<?php
  require_once("lib/jaguar/jaguar.inc.php");
  require_once("include/funcoes.inc.php");

  JDBAuth::SetValidated();

  $ManBD = ManBD::getInstance();
  $ManBD->objConn->SetDebug(0, 1);

  $title = "Extrato de Veículo";
  $html  = new JHtml($title);
  $html->AddHtml("<h3>$title</h3>");

  $consulta = new JConsultation($ManBD->objConn);

  //Veiculo
  $sql = <<<SQL
    SELECT ve.ds_placa,
           ve.ds_modelo,
           ve.ds_cor,
           vm.nm_marca
      FROM veiculo            ve
      LEFT JOIN veiculo_marca vm ON vm.cd_marca = ve.cd_marca
     WHERE ve.cd_veiculo = '{$_REQUEST["f_cd_veiculo"]}'
SQL;

  $consulta->AddArea("veiculo", $sql, "Veículo");

  $visibleFields = [
    "ds_placa"  => "Placa",
    "nm_marca"  => "Marca",
    "ds_modelo" => "Modelo",
    "ds_cor"    => "Cor"
  ];

  $cellOptions = ["colspan" => 3];

  $consulta->SetVisibleFields("veiculo", $visibleFields);

  $consulta->SetCellOptions("veiculo", "ds_cor",   $cellOptions);
  $consulta->SetCellOptions("veiculo", "ds_placa", $cellOptions);

  $consulta->SetNoBreakLine("veiculo", "ds_modelo");

  $consulta->SetCallback("veiculo", get_index_of($visibleFields, "ds_placa"), "Format_Placa", ["sys", "pt_BR"]);

  //Motorista
  $sql = <<<SQL
    SELECT pe.nm_pessoa,
           mo.nr_cnh,
           mo.dt_validade
      FROM motorista mo
      JOIN pessoa    pe ON pe.cd_pessoa = mo.cd_pessoa
     WHERE mo.cd_veiculo = '{$_REQUEST["f_cd_veiculo"]}'
SQL;

  $consulta->AddArea("motorista", $sql, "Motorista");

  $visibleFields = [
    "nm_pessoa"   => "Placa",
    "nr_cnh"      => "Nr. CNH",
    "dt_validade" => "Dt. Validade"
  ];

  $consulta->SetVisibleFields("motorista", $visibleFields);

  $consulta->SetCellOptions("motorista", "nr_cnh",      ["align" => "left"]);
  $consulta->SetCellOptions("motorista", "dt_validade", ["align" => "center"]);

  $consulta->SetCallback("motorista", get_index_of($visibleFields, "dt_validade"), "Format_Date", ["sys", "pt_BR"]);

  $html->AddObject($consulta);
  
  echo $html->GetHtml();
```