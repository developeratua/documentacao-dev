#Tabelas e formulários

Nesse tutorial é demonstrado a criação e a forma de utilização de tabelas e formulários

---

A construção do formulário fica a responsabilidade o objeto [JForm](../../jaguar/JForm), que realiza sua estruturação baseada em uma tabela, portanto, através dos métodos [OpenRow](../../jaguar/JTable#openrow), [OpenHeader](../../jaguar/JTable#openheader) e [OpenCell](../../jaguar/JTable#opencell), é possivel dispor objetos e textos em linhas, cabeçalhos e colunas, respectivamente.

A submissão e redefinição do formulário é realizada através do botões [JFormSubmit](../../jaguar/JFormSubmit) e [JFormReset](../../jaguar/JFormReset), respectivamente. Vale ressaltar que no arquivo [funcoes.inc.php](../../jaguar/funcoes) existe a função [fil_botao](../../jaguar/funcoes#fil_botao) que compromete a contruir esses dois botões automaticamente. A ação do formulário, ou seja, para onde ele vai mandar os dados, é definida atráves do método [SetAction](../../jaguar/JForm#setaction).

Quando um formulário envia dados para a mesma página em que foi disposto, é possível verificar seu envio através do método [IsSubmitted](../../jaguar/JForm#issubmitted).

No final, todo objeto deve ser inserido no objeto principal, no final deveremos ter uma hierarquia mais ou menos assim:

1. JHTML
	* JForm
		* JFormText
		* JFormNumber
		* JFormSubmit
		* JFormReset
	* JTable

```php
<?php
  session_start();
  require_once("lib/jaguar/jaguar.inc.php");

  JDBAuth::SetValidated();

  $title = "Tabelas e formulários";
  $html  = new JHtml($title);
  $html->AddHtml("<h3>{$title}</h3>");

  $form = new JForm();

  if (!$form->IsSubmitted())
  {
    $form->SetAction($_SERVER["PHP_SELF"]);

    $form->OpenRow();
    $form->OpenHeader("<b>Nome</b>");
    $form->OpenCell();
    $nm_pessoa = new JFormText("f_nm_pessoa");
    $nm_pessoa->SetSize(50);
    $nm_pessoa->SetTestIfEmpty(true, "Preencha o campo Nome!");
    $form->AddObject($nm_pessoa);

    $form->OpenRow();
    $form->OpenHeader("<b>Idade</b>");
    $form->OpenCell();
    $nr_idade = new JFormNumber("f_nr_idade");
    $nr_idade->SetMaxLength(3);
    $nr_idade->SetTestIfEmpty(true, "Preencha o campo Idade!");
    $form->AddObject($nr_idade);

    $form->OpenRow();
    $form->OpenCell("", ["colspan" => 2, "align" => "center"]);
    $bt_submit = new JFormSubmit("f_bt_sumbit");
    $bt_submit->SetValue("Enviar");
    $form->AddObject($bt_submit);

    $bt_reset = new JFormReset("f_bt_reset");
    $bt_reset->SetValue("Limpar");
    $form->AddObject($bt_reset);
  }
  else
  {
    $table = new JTable();

    $table->OpenRow();
    $table->OpenHeader("Nome");
    $table->OpenHeader("Idade");

    $table->OpenRow();
    $table->OpenCell($_REQUEST["f_nm_pessoa"]);
    $table->OpenCell($_REQUEST["f_nr_idade"]);

    $html->AddObject($table);
  }

  $html->AddObject($form);

  echo $html->GetHtml();
```