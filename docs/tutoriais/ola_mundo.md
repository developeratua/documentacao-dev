#Olá Mundo

Nesse pequeno tutorial será demonstrado como criar uma simples página utilizando o Jaguar.

---

Para inicar um página com Jaguar o primeiro passo é **iniciar a sessão** e incluir a biblioteca **jaguar.inc.php**.

O método estático **JDBAuth::SetValidated**, é utilizado para seja possível acessar a página sem que seja necessário realizar a autenticação de usuário. Esse método **não deve ser utilizado em produção**, exceto em caso expecíficos.

O objeto [JHtml](../../jaguar/JHtml), assim como [JMaintenance](../../jaguar/JMaintenance), é um do objeto utilizado para englobar a página como um todo. A saída deles resultará no interfáce da página ([GetHtml](../../jaguar/JPrimitiveObject#gethtml)).

Para inserir um código HTML utiliza-se da função [AddHtml](../../jaguar/JPrimitiveObject#addhtml) que, como o Jaguar monta o fonte da página de forma estruturada, adiciona-o naquele ponto de execução.

```php
<?php
  session_start();
  require_once("lib/jaguar/jaguar.inc.php");

  JDBAuth::SetValidated();

  $title = "Olá Mundo";
  $html  = new JHtml($title);
  $html->AddHtml("<h3>{$title}</h3>");

  echo $html->GetHtml();
```