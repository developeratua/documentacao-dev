#Listagens

Neste tutorial será mostrado o uso da classe JDBGrid para a criação de listagens. 

Nas telas de listagens são exibidas informações de determinadas tabelas do banco de dados contendo links que direcionam para a [Manutenção](/tutoriais/manutencoes) ou [Extrato](/tutoriais/extratos). Contém também campos para realizar a filtragem das informações que serão exibidas na [Grid](/tutoriais/manutencoes/#jdbgrid).

---

##JDBGrid

O **JDBGrid** é uma classe do jaguar para criação de grids para exibição de informações.

As colunas exibidas no Grid são montadas a partir de um array de relação **nome da coluna => header**, estes são chamados **visibleFields** e atrelados ao objeto **JDBGrid**.

A partir do método **AddExtraFields** são adicionadas colunas adicionais que contém campos com links para as manutenções ou extratos criados com os métodos **SetLink**, onde são informados os parâmetros para a manutenção que o link irá direcionar, e **SetLinkFields** que contém as chaves referentes a informação selecionada no Grid.

Os campos dos filros são criados a partir do método **AddFilterField** indicando nos parâmetros o campo e a operação necessária para a filtragem dos dados no banco.

Com o método **SetFilterProperties** é possivel estilizar um campo de filtragem específico.

```php
<?php
  session_start();
  require_once("lib/jaguar/jaguar.inc.php");
  require_once("include/funcoes.inc.php");

  JDBAuth::SetValidated();

  $conn = JDBConn::getInstance();
  $conn->SetDebug(0, 1);

  $title = "Listagem de Cidades";
  $html  = new JHtml($title);
  $html->AddHtml("<h3>{$title}</h3>");

  $sql           = [];
  $sql["fields"] = "c.cd_cidade, 
  					c.nm_cidade, 
  					e.ds_sigla ";
  $sql["count"]  = "c.cd_cidade ";
  $sql["from"]   = "cidade 		c 
  				    JOIN estado e ON c.cd_estado = e.cd_estado ";
  $sql["order"]  = "nm_cidade";

  $grid_cidade = new JDBGrid("grid_cidade", $conn, $sql);

  $visibleFields = [
    "nm_cidade" => "Cidade",
    "ds_sigla"  => "UF"
  ];

  $qtCampos = sizeof($visibleFields);

  $grid_cidade->SetVisibleFields($visibleFields);

  $grid_cidade->AddExtraFields([" " => "Propriedades"]);
  $grid_cidade->SetLink($qtCampos,       "man_cidade.php");
  $grid_cidade->SetLinkFields($qtCampos, ["f_cd_cidade" => "cd_cidade"]);

  $grid_cidade->AddFilterField("nm_cidade", "Cidade", "text", "~*");
  $grid_cidade->AddFilterField("ds_sigla",  "UF",     "text", "~*", "ds_sigla", false, false);
  $grid_cidade->SetFilterProperties("ds_sigla", ["SetMaxLength" => "2"]);

  $html->AddObject($grid_cidade);

  $html->AddHtml("<br><a href='man_cidade.php'>Adicionar Cidade</a>");

  echo $html->GetHtml();
```