#Padrões de alinhamentos
	
No sistema são necessários os seguir os padrões de alinhamento para que seja possível ter um código limpo e bem estruturado.
	
---

##Alinhamento de níveis

É chamado de alinhamento de nível quando um bloco de código abrange vários outros dentro de si com o intúito de realizar o afunilamento do código. Esse tipo de formatação é impressindíve a fim de compreender o fluxo do programa.

	
```php
<?php
  if (1 == 1)
  {
    if (2 == 2)
    {
      if (3 == 3)
      {
        if (4 == 4)
        {
          if (5 == 5)
          {
            for ($i = 1; $i <= 5; $i++)
              echo "Eu passei pelos números {$i}";
          }
        }
      }
    }
  }

```

Por padrão é definido que para cada nível de afunilamento seja disponibilizado **um tab** de distância, esse por sua vez, configurado com **dois espaços**.

##Alinhamento de inicialização/atribuição de variáveis

É recomendado a criação de blocos de inicialização de variáveis, para que, em um código muito extenso, seja possível localiza-las facilmente. 
Por padrão, as variáveis devem **sempre** ser inicializadas uma a cima da outra, e os "**=**" que as atribuem um valor devem estar alinhados.

Ex:

```php
<?php
	$valor            = 1.20;
	$quantidade       = 42;
	$nomeDestinatario = "Cremilson da Silva";
	...
```

##Alinhamento de chaveamento

Na padronização do sistema, o esquema de chaveamento é feito sempre na linha inferior (Allman style) da declaração da condição, função, etc.. São excessões nos casos onde é feito o uso de **callbacks**, quando isso ocorre é utilizado o estilo (K&R), ou seja, abertura de chaves na mesma linha e o fechamento alinhado com o início da declaração da função.

Ex:

```php
<?php
  if ($_REQUEST["f_id_tipo"] == 1)
  {
    $Cremilson = array_filter($_REQUEST["arr_pessoas"], function ($el) {
     return $el == "Cremilson da Silva";
    });
  }
```

##Alinhamento de dados

Dentro do sistema, existe uma padronização de alinhamento de dados, independentemente onde eles estejam, seja num relatório, extrato, manutenção, devem seguir o seguinte padrão:

| Tipo      | Alinhamento  |
| --------- | :----------: |
| string    | esquerda     |
| float     | direita      |
| int       | direita      |
| date      | centro       |
| timestamp | centro       |
