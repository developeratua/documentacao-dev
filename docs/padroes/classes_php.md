##Classes PHP

---

##Criação de classes

Por padrão a criação de classes é algo automatizado pois a criação delas é baseada a partir de uma **tabela** no banco de dados, portanto, a tarefa de criação fica a responsabilidade do processo [exe_GeradorClassesManBD](../../jaguar/GeradorClassesManBD). 

```php
<?php
  class ClasseTabela
  {
    /**
      * <b>infoBD:</b> integer(32) not null<br />
      * @var int
      */
    public $cd_tabela;

    /** @var array */
    public $key;

    /** @var string */
    public $table;

    /** @var JDBConn */
    public $objConn;

    /** @param JDBConn */
    public function __construct(JDBConn $objConn)
    {
      $this->objConn = $objConn;
      $this->table   = "tabela";
      $this->key     = array("cd_tabela");
    }    
  }

```

##Opções para campos id_

Geralmente os campos id_ são utilizados para definir tipos e/ou categorias. Como no banco só guardamos números para esses dados, se faz necessário adicionar no PHP a **representação do valor**, para que seja possível identificar o valor real da referência na hora do desenvolvimento. Para isso seguimos o seguinte padrão:

1. Definir constântes com nome mnemônico da representação real do valor e prefixo com nome da coluna no banco;
2. Criar uma array público e estático contendo os valore e as descrições do campo. O nome do array deve ter o prefixo opId.

```php
<?php
  class Telefone
  {
    /**
     * <b>infoOP:</b> self::$opIdTipo<br />
     * <b>infoBD:</b> smallint(16) not null<br />
     * @var int
     */
    public $id_tipo;

    const ID_TIPO_FIXO    = 1;    
    const ID_TIPO_CELULAR = 2;

    public static $opIdTipo = [
      self::ID_TIPO_FIXO    => "Fixo",
      self::ID_TIPO_CELULAR => "Celular",
    ];
  }
```

Com isso podemos ter um código mais fácil e legivel, desde popular um [JFormSelect](../../jaguar/JFormSelect) até realizar testes.

```php
<?php
  // id_tipo
  $id_tipo = new JFormSelect("f_id_tipo");
  $id_tipo->SetOptions(Telefone::$opIdTipo);
  $id_tipo->SetDefaultValue(Telefone::ID_TIPO_CELULAR);

  if ((int)$Tefone->id_tipo === Telefone::ID_TIPO_FIXO)
    echo "Telefone fixo";
```

##Métodos

Na criações de métodos de uma classe se faz necessário seguir a seguinte padronização:

1. Definir a visibilidade do método;
2. Definir se o método é estático ou não;
3. Adicionar a palavra-chave function;
4. Definir o nome da função
	* Deve estar no padrão **camelCase**;
	* Deve estar no infinitivo;
5. Inicializar os parâmetros
6. Gerar o PHPDoc;
7. Documentar o funcionamento do método.

```php
<?php
  /**
   * Retorna um objeto Pessoa a partir do nº de cpf
   * 
   * @param int $nrCpf
   * @return \Pessoa
   */
  public static function obterNomePessoa($nrCpf = null)
  {
    $ManBD = ManBD::getInstance();

    $Pessoa = new Pessoa($ManBD->objConn);
    $Pessoa->nr_cnpj_cpf = $nrCpf;
    $ManBD->PopulaObjetoGenerico($Pessoa);

    return $Pessoa;
  }

  /**
   * Define o nome na instância do objeto Pessoa
   * 
   * @param string $nmPessoa
   */
  private function definirNomePessoa($nmPessoa = "")
  {
    $this->nm_pessoa = $nmPessoa;
  }
```