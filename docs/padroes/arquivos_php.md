#Arquivos PHP

---
##Nomenclatura de arquivo PHP

Todos os arquivos contidos nos projetos possuem nomenclaturas que representam determinadas funções de acordo com um processo do sistema. Os arquivos são nomeados com prefixos como **"exe_"**, **"sel_"**, **"man_"**, entre outros:

###Arquivos "exe_":

###Arquivos "sel_", "man_", "md_", "con_":

Esses arquivos pazem parte das [Manutenções](/tutoriais/manutencoes) que são áreas do sistema responsáveis por gerenciar o conteúdo de áreas específicas.

1. **"sel_:"** São as listagens dos dados de determinada tabela, contendo filtros e links para sua respectiva manutenção;
2. **"man_:"** Contém a interface para o gerenciamento das informações de uma determinada tabela através da classe [JMaintenance](/tutoriais/manutencoes/#jmaintenance);
3. **"md_:"** Contém a relação de uma ou mais tabelas do banco que contém relações com a tabela principal. [JMasterDetail](/tutoriais/manutencoes/#jmasterdetail);
4. **"con_:"** São os [Extratos](/tutoriais/extratos/) que são páginas onde são exibidos todos os dados de uma determinada tabela e/ou conjunto de tabelas relacionadas.

###Arquivos "fil_", "rel_":

1. **"fil_:"** Página que contém os filtros para a geração de um determinado relatório;
2. **"rel_:"** Arquivo que contem as funções de tratamento para a geração de um relatório, sendo um arquivo PDF, XLS

##Inicialização de um arquivo PHP

Todo arquivo criado seja ele processo, listagem, manutenção, extrato, filtro, deve seguir o seguinte padrão de inicialização:

1. Iniciar a sessão;
2. Incluir a biblioteca Jaguar;
3. Incluir o arquivo [funcoes.inc.php](../../jaguar/funcoes);
4. Obter uma instância do objeto [JDBConn](../../jaguar/JDBConn), ou [ManBD](../../jaguar/ManBD);
5. Chamar o método [SetDebug](../../jaguar/JDBConn#setdebug);

```php
<?php
  session_start();
  require_once("lib/jaguar/jaguar.inc.php");
  require_once("include/funcoes.inc.php");

  $conn = JDBConn::getInstance();
  $conn->SetDebug(0, 1);

```

##Declarações de funções em um arquivo PHP

Algumas vezes se faz necessário a declaração de uma função para que seja possível realizar uma ação específica que vai ocorrer somente no arquivo em questão, caso contrário, deve-se criar um método na classe que realiza a ação ou na classe relacionada com o processo, manutenção, etc..

As funções em um arquivo devem ser declaradas depois da instância do $conn e antes da instância do objeto principal do arquivo (entende-se por objeto prinpal aquele que irá manter dentro de sí todos os objetos instânciados, por exemplo JHtml ou JMaintenance).

```php
<?php
  session_start();
  require_once("lib/jaguar/jaguar.inc.php");
  require_once("include/funcoes.inc.php");

  function obterValores()
  {
    $a = 1;
    $b = 2;

    return $a + b;
  }

  $conn = JDBConn::getInstance();
  $conn->SetDebug(0, 1);

  $html = new JHtml();

```