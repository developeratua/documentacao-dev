#SQL

---

##Consultas

As regras de alinhamento de consulta, e de basicamente todos os comandos SQL, devem ser:

1. As cláusulas **FROM**, **JOIN**, **LEFT JOIN**, **WHERE**, **ORDER BY**, **GROUP BY**... devem estar todas alinhadas com o **SELECT**;
2. Os campos selecionados devem estar um abaixo do outro;
3. Em toda consulta deve exisir um alias para cada tabela, preferencialmente com as duas primeiras letras da tabela;
4. Os "**AS**" dos campos selecionados devem estar alinhados;
5. Todas as palavras-chave do SQL devem estar em caixa alta;
6. Nos **JOINS**, a chave da tabela que será relacionada deve ser declarada primeiro;
7. As ligações das tabelas devem ser alinhadas pelo "**=**" da chave com maior nome;
8. Os alias das tabelas devem estar sempre alinhados, levando em consideração a tabela com maior nome;
9. Declarações de tabelas with devem possuir o prefixo "**with_**";


```sql
WITH with_obter_marca_veiculo AS (
  SELECT ve.cd_veiculo, 
         vm.nm_marca
    FROM veiculo            ve
    LEFT JOIN veiculo_marca vm ON vm.cd_marca = ve.cd_marca
)

SELECT pe.nm_pessoa,
       COALESCE(pe.ds_apelido, 'Não possui') AS ds_apelido,
       ci.nm_cidade || ' / ' || es.ds_sigla  AS ds_cidade_estado
  FROM pessoa                          pe
  JOIN cidade                          ci ON    ci.cd_cidade = pe.cd_cidade
  JOIN estado                          es ON    es.cd_estado = ci.cd_estado
                                         AND     es.id_ativo = 1
  LEFT JOIN veiculo                    ve ON ve.cd_motorista = pe.cd_pessoa
  LEFT JOIN with_obter_marca_veiculo womv ON womv.cd_veiculo = ve.cd_veiculo
 WHERE pe.cd_pessoa = 12
 ORDER BY pe.nm_pessoa, ci.nm_cidade
```

##Atualizações

As regras de atualização não diferem das diretrizes das consultas, vide o exemplo:

```sql
UPDATE cidade ci
   SET ci.nm_cidade = 'Nova Cidade'
 WHERE ci.cd_cidade = (SELECT ci_.cd_cidade
                         FROM cidade ci_
                        WHERE ci_.cd_pessoa = 123);
```

Nesse ponto vale ressaltar a questão de subconsultas, estas devem ser alinhadas a partir do momento de sua declaração e seus alias devem conter o sufixo "**_**".

##Funções

A criação de funções diratamente no banco é algo raro de acontecer, porém, vale ressaltar sua padronização:

1. O nome da função deve ter o prefixo "**fun_**";
2. Alinhamento de dois espaços, ou um tab;


```sql
CREATE OR REPLACE FUNCTION fun_data_hora_atual()
  RETURNS TEXT AS
$$
BEGIN
  RETURN TO_CHAR(CURRENT_TIMESTAMP, 'DD/MM/YY HH24:MI:SS');
END;
$$
LANGUAGE plpgsql;

```

##Triggers

Assim como funções,  são raras as criações de triggers diretamente no banco.

1. O nome da trigger deve iniciar com o prefixo "**trigger_**";
2. Alinhamento de dois espaços, ou um tab;

###Variáveis Especiais

Quando uma função é chamada como uma trigger, algumas variáveis são criadas automaticamente. Exemplos:

1. **NEW:** Guarda os novos dados para operações de **INSERT/UPDATE**;
2. **OLD:** Guarda os dados antigos para operações de **UPDATE/DELETE**;
3. **TG_OP:** String que armazena **"INSERT"**, **"UPDATE"**, **"DELETE"**, ou **"TRUNCATE"** dizendo qual dessas operações disparou a trigger;

[Documentação do PostgreSQL](https://www.postgresql.org/docs/9.2/plpgsql-trigger.html).

```php
CREATE OR REPLACE FUNCTION fun_nome_funcao()
  RETURNS TRIGGER AS
$$
DECLARE
  new_codigo INTEGER;
  old_codigo INTEGER;
BEGIN
  IF (TG_OP = 'INSERT') THEN
    new_codigo := NEW.cd_codigo;

    -- faz algo --

    RETURN NEW;
  ELSEIF (TG_OP = 'DELETE') THEN
    old_codigo := OLD.cd_codigo;

    -- faz algo --

    RETURN OLD;
  ELSEIF (TG_OP = 'UPDATE') THEN
    new_codigo := NEW.cd_codigo;
    old_codigo := OLD.cd_codigo;

    -- faz algo --

    RETURN NEW;
  END IF;
END;
$$
LANGUAGE plpgsql;

CREATE TRIGGER trigger_nome_trigger
  AFTER INSERT OR UPDATE OR DELETE ON nome_tabela
    FOR EACH ROW
EXECUTE PROCEDURE fun_nome_funcao();
```