#Variáveis
---

##Nomenclatura

###Prefixos

Ao criar uma variável, principalmente quando essa vai armazenar um valor vindo diretamente do banco de dados, é necessário defini-las com um padrão de prefixos que definem o tipo de valor o qual elas armazenam.

| Prefixo | Tipo             | Valor de expressão                                                            | Exemplo       |
| ------- | :--------------: | ----------------------------------------------------------------------------- | ------------- |
| arr     | Array            | Array                                                                         | arr_pneus     |
| cd      | Int              | Geralmente define a chave primaria de algo que vem do banco de dados          | cd_produto    |
| dt      | date / timestamp | Data ou data e hora                                                           | dt_cadastro   |
| ds      | String           | Descrição                                                                     | ds_texto      |
| hr      | Time             | Hora                                                                          | hr_cadastro   |
| id      | Int              | Geralmente utilizado para definir status, tipos, etc...                       | id_formato    |
| nm      | String           | Geralmente utilizado para definir algo possua um nome                         | nm_pessoa     |
| nr      | Int              | Número inteiro                                                                | nr_casa       |
| obj     | Object           | Geralmente utilizado para definir um objeto JS ou PHP stdClass                | obj_pneu      |
| opId    | Array            | Array de opções para popular o objeto [JFormSelect](../../jaguar/JFormSelect) | op_id_sim_nao |
| pr      | Float            | Percentual de algo                                                            | pr_gastos     |
| qt      | Int              | Quantidade de algo                                                            | qt_produto    |
| vl      | Float            | Número decimal                                                                | vl_produto    |

##Declarações de variáveis

###Declaração de objetos Jaguar

Ao instanciar um novo **objeto Jaguar**, deve-se seguir seguinte padrão:

1. Utilizar separação de palavras com o padrão **underscore**;
2. Definir o prefixo conforme o valor que será obtido/salvo no banco ou apenas utilizado na variável;
3. No **name** do objeto, adicionar o prefixo **f_**;

```php
<?php
  $cd_pessoa     = new JFormNumber("f_cd_pessoa");
  $nm_pessoa     = new JFormText("f_nm_pessoa");
  $dt_nascimento = new JFormDate("f_dt_nascimento");
```

###Declaração de variáveis comuns

São consideradas variáveis comuns toda e qualquer variável que não armazena um objeto, indiferente da natureza desse.

1. Utilizar separação de palavras com o padrão **camelCase**;
2. Definir o prefixo conforme o valor que será armazenado na variável

```php
<?php
  $nmPessoa      = "Dino da Silva Sauro";
  $prAcrescimo   = 30.00;
  $vlSaldoMensal = 123.12;
```

###Declaração de objetos

Ao instanciar um novo objeto, as seguintes regras valem para a declaração da variável que irá o armazenar:

1. A variável deve iniciar com letra em caixa alta;
2. Utilizar separação de palavras com o padrão **camelCase**;
3. A variável deve ter o mesmo nome do objeto (quando possível);
3. Na maioria das vezes se faz necessário passar por parâmetro no construtor da classe o objeto [JDBConn]();

```php
<?php
  //Se já existir, não é preciso obter uma nova instância 
  //de um objeto ManBD quando for instanciar um objeto.
  $ManBD = ManBD::getInstance();

  $ClasseTabela = new ClasseTabela($ManBD->objConn);

```








