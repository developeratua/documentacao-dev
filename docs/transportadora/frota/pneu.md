#**Pneu**

Caminho: **Frota  >>  Pneu**

O objetivo desse módulo é realizar o controle dos pneus da empresa, entende-se como controle, o cadastro de pneus adquiridos, a movimentação dos mesmos dentro da frota, a situação ao qual se encontra e a manutenção.

##Cadastro

Caminho: **Frota  >>  Pneu  >>  Cadastro  >>  Dados do Pneu**

Quando um pneu é adquirido pela transportadora, ele deve ser registrado no sistema. O registro de um pneu é baseado nos seguintes dados:

| Campo              | Descrição                                                                                                     |
| ------------------ | ------------------------------------------------------------------------------------------------------------- |
| Nr. Fogo           | É um número utilizado para o controle da vida útil do pneu dentro da organização da oficina da transportadora.|
| Bitola             | Referente as medidas do pneu, essas previamente cadastradas em [Bitola de Pneu](../cadastro/bitola_pneu).     |
| Série              | Define se o formulário deve ser focado na sua criação                                                         |
| Marca              | Marca do pneu. As marcas são previamente cadastradas em [Marcas de Pneu](../cadastro/marca_pneu)              |
| Tipo Borracha      | É o tipo de material que constinue o pneu, podendo ser Liso, Borrachudo ou Misto.                             |
| Banda Rodagem      | É a área do pneu que entra em contato com o solo, responsáveis pela aderência do veículo.                     |
| Modelo             | O modelo de pneu geralmente esta atrelado a sua marca, tendo suas próprias caracteristicas.                   |
| Dt. Compra         | Indica a data em que a transportadora adquiriu o pneu.                                                        |
| Vl. Compra         | Indica o valor pago pelo pneu.                                                                                |
| Observação         | Nessa área são indicadas as caracterísicas ou informação que não puderam ser descritas nos campos anteriores. |

Depois de um período, a transportadora pode entender que o pneu já se encontra obsoleto, podendo então descarta-lo ou vende-lo. Para fazer isso, define-se os seguintes campos:

| Campo              | Descrição                                                                                                                                            |
| ------------------ | -----------------------------------------------------------------------------------------------------------------------------------------------------|
| Dt. Descarte       | Indica a data em que o pneu foi deixado de ser utilizado.                                                                                            |
| Motivo Descarte    | Indica a razão pela qual o pneu não é mais utilizado.                                                                                                |
| Vl. Venda          | Caso o motivo do descarte seja referente a venda do mesmo, nesse campo se indica o valor pelo qual o mesmo foi trocado.                              |
| Agência Descarte   | Geralmente um empresa tem várias agências, e os veículos transitam entre elas, nesse campo, define-se em qual delas o pneu foi descartado ou vendido.|
| Motorista Descarte | Indica o motorista do veículo em qual o pneu estava montado quando o pneu teve de ser descartado.                                                    |

Geralmente quando um pneu é adquirido, o mesmo é atrelado a uma [despesa](), um registro de que foi efetuado a compra desse ou de um conjunto de pneus. Logo quando o pneu é adquirido também é possível indicar onde ele se encontra fisicamente. Esses dados são informados nos campos:

| Campo              | Descrição                                                                                |
| ------------------ | -----------------------------------------------------------------------------------------|
| Despesa            | Indica o registro de compra do pneu. Você pode ter mais detalhes sobre despesas [aqui]().|
| Armazem            | Indica a filial onde o pneu se encontra fisicamente. Vale ressaltar que só se pode definir o armazem do pneu caso o mesmo não esteja montado em um veículo.|

![alt text](../../img/frota/pneu/manutencao_pneu.png "Dados do Pneu")

Um pneu possui **situações de estoque**, essas podem ser definidas como:

- Montado: Define que o pneu está montando em um [veículo da frota]() e está rodando. Essa situação é definida automaticamente quando o usuário relaciona o pneu com um veículo em uma das telas de [movimentação]();

- Em estoque: Define que o pneu está desmontado, podendo esse estar estocado em um [armazém](). Essa situação é definida automaticamente quando o usuário remove o relacionamento do pneu com um veículo ou quando um pneu volta de uma manutenção;

- Em Manutenção: Define que o pneu esta desmontado e foi enviado à manutenção para algum reparo ou outro serviço. Essa situação é definida automaticamente quando o usuário abre uma manutenção para o pneu;

- Descartado: Define que um pneu esta obsoleto e não será mais usado, ou que foi vendido. Essa situação é definida automaticamente quando o usuário define o campo **Dt. Descarte**. Quando um pneu estiver descartado, ele fica impossibilitado de realizar ações como, manutenção, movimentação, etc...

##Manutenção

Caminho: **Frota  >>  Pneu  >>  Cadastro  >>  Manutenção de Pneu  >>  Manutenção  >>  Dados das Manutenções do Pneu**

Quando um pneu estraga ou é comprado com algum defeito, ele é enviado para manutenção para que seja possível consertar a falha existente. Também é possível abrir uma manutenção para realizar outros serviçoes relacionados ao pneu, como por exemplo, a movimentação de rodízio em um veículo.

O processo de manutenção é dividido em três partes: **Informações gerais da manutenção**, **Dados de Envio** e **Dados de Retorno**.

Na primeira parte, são informados os dados essenciais para se abrir uma manutenção:

| Campo              | Descrição                                                                                                                           |
| ------------------ | ------------------------------------------------------------------------------------------------------------------------------------|
| Pneu               | Representa o pneu em que será realizado o serviço.                                                                                  |
| Fornecedor         | Indica a pessoa/empresa responsável pelo conserto ou serviço.                                                                       |
| Data               | Indica a data de abertura da manutenção.                                                                                            |
| Tipo Manutenção    | Repesenta o tipo de conserto ou outro serviço realizado nessa manutenção.                                                           |
| Motivo             | Indica o motivo pelo qual essa manutenção teve de ser aberta.  Esses motivos são cadastrados na [Manutenção de Motivo de Descarte](../cadastro/motivo_descarte)|
| Observação         | Nessa área são indicadas as caracterísicas ou informação que não puderam ser descritas nos campos anteriores.                       |
| Despesa            | Para todo serviço é realizado uma cobrança, nessa área é relacionado a [despesa]() que foi gerada para a realização essa manutenção.|

Com essas informações, já é possível abrir uma manutenção, entretando, é possível controlar melhor a integridade do pneu utilizando a área de **Dados de Envio**. Nessa área, são inidicadas as características com as quais o pneu foi enviado para manutenção.

| Campo                   | Descrição                                                                                                                           |
| ----------------------- | ------------------------------------------------------------------------------------------------------------------------------------|
| Dt. Envio               | Determina a data que o pneu foi enviado para a manutenção.                                                                          |
| Dt. Previsão de Entrega | Indica uma data estipulada para o retorno do pneu.                                                                                  |
| Km Atual Veículo        | Indica a quilometragem em que o véiculo, em qual o pneu estava montado, no momento em que o pneu foi enviado para a manutenção.     |
| Medida Sulcagem (mm)    | Indica a altura das "ranhuras" do pneu no momento em que ele foi enviado para a manutenção                                          |
| Modelo                  | Indica o modelo de pneu que foi enviado para a manutenção.                                                                          |
| Tipo Borracha           | Indica o tipo de borracha do pneu que foi enviado para a manutenção.                                                                |
| Despesa                 | Para todo serviço é realizado uma cobrança, nessa área é relacionado a [despesa]() que foi gerada para a realização essa manutenção.|

Quando um pneu volta da manutenção, existe a possíbilidade de algumas de suas características mudarem. Na área de **Dados de Retorno**, informa-se a situação em que o pneu se encontra pós manutenção, além de alguns dados de registro.

| Campo                   | Descrição                                                                                                                           |
| ----------------------- | ------------------------------------------------------------------------------------------------------------------------------------|
| Nr. Documento Retirada  | Indica um número de controle interno da própria transportadora.                                                                     |
| Dt. Entrega             | Indica a data em que o pneu voltou da manutenção. Obs: essa informação pode divergir da "**Dt. Previsão de Entrega**".              |
| Valor                   | Indica o custo da manutenção.                                                                                                       |
| Modelo                  | Indica o modelo de pneu retornado da manutenção.                                                                                    |
| Tipo Borracha           | Indica o tipo de borracha do pneu retornado da manutenção.                                                                          |
| Despesa                 | Para todo serviço é realizado uma cobrança, nessa área é relacionado a [despesa]() que foi gerada para a realização essa manutenção.|

![alt text](../../img/frota/pneu/manutencao_manutencao_pneu.png "Dados das Manutenções do Pneu")

##Movimentação

O processo de movimentação de pneus pode ser resumido como uma forma de realizar a relação veículo/pneu.

Esse processo pode ser realizado de três formas diferentes:

###Pneu/Veículo

Caminho: **Frota  >>  Pneu  >>  Cadastro  >>  Manutenção de Pneu  >>  Movimentação de Pneu  >>  Movimentação do Pneu**

Através da tela de **Movimentação de Pneu** é possível relacionar, um deterimando veículo ao pneu condizente durante um determinado período. Vale ressaltar, que não é possível abrir uma movimentação, quando outra ainda não estiver fechada.

| Campo                   | Descrição                                                                                                                           |
| ----------------------- | ------------------------------------------------------------------------------------------------------------------------------------|
| Pneu                    | Representa o pneu em que será movimentado.                                                                                          |
| Placa                   | Indica a placa do veículo em que o pneu será montado.                                                                               |
| Posição do Pneu         | Indica a roda do veículo em que o pneu será montado.                                                                                |
| Data Montagem           | Indica o dia em que o pneu foi colocado no veículo.                                                                                 |
| Km Montagem             | Indica a quilometragem do pneu no momento em que ele é disposto no veículo.                                                         |
| Data desmontagem        | Indica o dia em que o pneu foi removido no veículo.                                                                                 |
| Km Desmontagem          | Indica a quilometragem do pneu no momento em que ele é removido do veículo.                                                         |

###Veículo/Pneu

Caminho: **Frota  >>  Veículo Próprio  >>  Manutenção de Veículo Próprio  >>  Pneu  >>  Manutenção de Pneus dos Veículos**

Segue a mesma lógica da tela exibida anteriormente, a diferença é que a montagem ocorre na manutenção do veículo. Os campos, exceto "Placa", que é trocada por "Pneu", e as regras são as mesmas da tela descrita no item acima.

###Movimentação

Caminho: **Frota  >>  Pneu  >>  Movimentação**

Diferente das outras duas telas, que são **indicadas para movimentações específicas**, a tela de movimentação tenta trazer uma experiência mais próxima a realidade, aplicando diversas regras, que na manutenção podem ser ignoradas. 

Vale ressaltar que, para ser possível a realização desse processo, é preciso ter definido o mapeamento da posição pneus do tipo de veículo, esse disposto na [manutenção de tipos de veículo]().

O processo de movimentação, inicia a partir da escolha do veículo onde os pneus serão alocados ou deslocados.

![alt text](../../img/frota/pneu/movimentacao_pneu_veiculo_01.png "Movimentação de Pneus do Veículo")

A tela seguinte é formada por três áreas: **Veículos**, **Pneus do Veículo** e **Alocação interativa de pneus**.

![alt text](../../img/frota/pneu/movimentacao_pneu_veiculo_02.png "Movimentação de Pneus do Veículo")

####Veículos

Nessa área são listados, além do veículo que foi filtrado, o seu conjunto ativo, dessa forma, o processo de movimentação de pneus acaba se tornando mais prático.

No final de cada linha dessa área existe um ícone de caminhão, ao clicar nele, é selecionado o veículo ao qual os pneus serão relacionados ou desrelacionados.

#####Pneus do Veículo

Essa área é responsável pela listagem da posição dos pneus e os os pneus que estão relacionados a elas. As posições dos pneus no veículo são definidas na [Manutenção de Posição Pneu Tipo Veículo]() dentro da [Manutenção de Tipo de Veículo]().

#####Alocação interativa de pneus

Essa é a área mais importante do processo, nela ocorre a movimentação dos pneus.

A ímagem no centro dessa área representa o tipo do veículo em que os pneus serão alocados. A área que representa os pneus nessa imagem podem estar em três situações:

- **Não mapeada**: Representado pela cor **cinza**. As posições nessa situação não possuem interação;

- **Não alocada**: Representado pela cor **vermelha** e com três pontos brancos. As posições nessa situação representam que **não existe** um pneu alocado nessa posição;

- **Alocada**: Representado pela cor **cinza escuro** e com o **Nr. Fogo**. As posições nessa situação representam que **existe** um pneu alocado nessa posição;

Para alocar um pneu em uma determinada posição, basta realizar **dois cliques** com o mouse sobre uma área não alocada, dessa forma, será exibido um modal para selecionar o pneu que será definido na posição.

![alt text](../../img/frota/pneu/movimentacao_pneu_veiculo_03.png "Modal de relação de pneu")

Também é possível realocar a posição de um pneu, arrastando um pneu alocado para uma área não alocada. Ao fazer isso, o pneu movimentado terá sua **data de desmontagem**, na posição anterior, e a **data de montagem**, na nova posição, definidas como a data atual.

Nessa tela também é possível **estocar**, **enviar para manutenção** ou **descartar** um pneu, arrastando um pneu montada para um dos ícones dispostos no canto superior direito, esses com as respectivas funções. Ao arrastar será exibido um modal com os mesmos campos das manutenções.

Ao realizar dois clique sobre um pneu montado, é possível realizar o **descarte e substituição** ou **estoque e substituição** de um pneu, de forma consecutiva.

![alt text](../../img/frota/pneu/movimentacao_pneu_veiculo_04.png "Modal de descarte ou estoque e substituição de pneu")