#Motivos de Descarte / Manutenção de Pneu

Caminho: **Frota  >>  Pneu  >>  Motivo Descarte  >>  Manutenção de Motivo de Descarte / Manutenção de Pneu**

Nessa tela são cadastrados os motivos pelos quais um pneu é descartado ou mandado para a manutenção.

![alt text](../../../img/frota/cadastro/motivo_descarte.png "Motivos de Descarte")