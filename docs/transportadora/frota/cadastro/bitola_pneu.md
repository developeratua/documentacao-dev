#Manutenção de Bitola de Pneu

Caminho: **Frota  >>  Pneu  >>  Bitola de Pneu  >>  Manutenção de Bitola de Pneu**

Nessa tela são cadastradas as informações relacionadas as medidas do pneu:

| Campo                   | Descrição                                        |
| ----------------------- | -------------------------------------------------|
| Bitola                  | Indica a largura nominal do pneu em milímetros.  |
| Perfil                  | Relação entre largura e a altura nominal do pneu.|
| Aro                     | Indica o diâmetro inteno do pneu, em polegadas.  |

Geralmente essas informações estão disponstas na lateral do pneu no formato **205/55 R 16 91 W**, porém o sistema só considera os primeiros quatro valores.