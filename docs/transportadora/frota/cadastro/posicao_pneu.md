#Manutenção de Posição do Pneu

Caminho: **Frota  >>  Pneu  >>  Posição de Pneu  >>  Manutenção de Posição do Pneu**

Nessa tela são cadastradas as informações referentes a posição de um pneu em relação a um veículo.

![alt text](../../../img/frota/cadastro/posicao_pneu.png "Posição do Pneu")

#Tipo de Veículo

Caminho: **Manutenção de Tipo Veículo Posição**

Nessa tela é possível definir quais os [tipos de veículos](../../../veiculo/cadastro/tipo_veiculo) em que é possível encontrar essa posição.

![alt text](../../../img/frota/cadastro/tipo_veiculo.png "Tipo de Veículo")