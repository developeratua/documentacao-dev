#Marcas de Pneu

Caminho: **Frota  >>  Pneu  >>  Marca de Pneu  >>  Listagem de Marcas de Pneu**

Processo de cadastro das marcas distribuidoras de pneu;

![alt text](../../../img/frota/cadastro/marca_pneu.png "Marcas de Pneu")

#Banda de Rodagem

Caminho: **Frota  >>  Pneu  >>  Marca de Pneu  >>  Manutenção de Marca de Pneus  >>  Banda Rodagem  >>  Manutenção de Banda de Rodagem**

Cada marca pode ter um tipo diferente de banda de rodagem, essa é definida pelo tipo da borracha e pelo formato do desenho.

![alt text](../../../img/frota/cadastro/banda_rodagem.png "Banda de Rodagem")
