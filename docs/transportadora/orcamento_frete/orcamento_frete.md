#Orçamento de Frete

O processo de orçamento de frete tem como objetivo calcular, com base nas despesas que a transportadora irá ter durante o transporte, como por exemplo, o pagamento do motorista e impóstos sobre o transport, e com a aplicação de um lucro sobre carregamento, o valor do frete de um ou mais produtos.

Entende-se que um orçamento de frete pode conter vários orçamentos para carregamentos diferentes, esses com produtos diferetes, quantidades diferentes, meios de transpote diferentes, rotas diferentes, etc... Com isso, no futuro o usuário poderá realizar a [emissão de um lote de carregamento]() com base nesses dados.

##Solicitação de Orçamento de Frete

Caminho: **Emissão Doc.  >>  Orçamento de Frete  >>  Orçamento de Frete**

A primeira etapa do processo de orçamento de frete é a solicitação. Nessa parte serão dispostas todas as informações a respeito do frete.

As informações necessárias estão dispostas em oito áreas: **Dados do Orçamento**, **Dados do Produto**, **Dados do Veículo**, **Itens de Custo**, **Impostos**, **Markup**, **Valores do Orçamento** e **Totais**. Além dessas oito áreas, existe a de **Produtos do Orçamento**, que realiza o controle dos produtos/carregamentos já informados.

![alt text](../../img/orcamento_frete/orcamento_frete_02.png "Produtos do Orçamento")

###Dados do Orçamento

Nessa área o usuário basicamente definie quem é o solicitante/pagador do orçamento, de onde e para onde o frete será feito. Os demais campos são campos de controle do orçamento.

| Campo              | Descrição                                                                                                         |
| ------------------ | ------------------------------------------------------------------------------------------------------------------|
| Nr. Orçamento      | Exibe o código identificador do orçamento.                                                                        |
| Pagador            | Representa o solicitante/pagador do orçamento.                                                                    |
| Dt. Orçamento      | Indica a data de abertura do orçamento.                                                                           |
| Cidade Origem      | Local de partida do frete.                                                                                        |
| Dt. Orçamento      | Indica a data de abertura do orçamento.                                                                           |
| Cidade Destino     | Local de entrega do frete.                                                                                        |
| Situação           | Indica se o estado do orçamento. Quando é criado um orçamento de frete, sua situação padrão é **Aberto**, essa pode ser alterada por um usuário autoriazado, que irá definir se o frete será ou não realizado, dessa forma, poderá alterar alterar sua situação para **Aprovado** ou **Não Aprovado**. Quando a sistuação for definida como **Aprovado** ou **Não Aprovada**, não será possível realizar alterações nesse orçamento.|
| Observação         | Nessa área são indicadas as caracterísicas ou informação que não puderam ser descritas nos campos anteriores.     |

![alt text](../../img/orcamento_frete/orcamento_frete_01.png "Dados do Orçamento")

###Dados do Produto

Dentro dessa parte é informado o [produto que será transportado](), assim como o valor desse produto e suas dimensões.

![alt text](../../img/orcamento_frete/orcamento_frete_03.png "Área de Produto")

###Dados do Veículo

Nessa etapa o usuário deverá informar o conjunto de veículos que realizarão o carregamento, além da quantidade de conjuntos necessária, a distância da rota e o tempo de viagem.

| Campo              | Descrição                                                                                                             |
| ------------------ | ----------------------------------------------------------------------------------------------------------------------|
| Veículo            | Nesse campo é disposto o [conjunto de tipos de veículo](), que ficarão responsáveis pelo transporte da carga.         |
| Quantidade         | Representa a quantidade de conjuntos.                                                                                 |
| Qt. Km Ida         | Indica, em quilômentros, a distância entre o ponto de partida do corregamento até local de entrega.                   |
| Qt. Dias Ida       | Indica, em dias, o tempo necessário para percorrer do ponto de origem do veículo corregamento até local de entrega.   |
| Qt. Km Volta       | Indica, em quilômentros, a distância entre o ponto de partida do corregamento até local de entrega.                   |
| Qt. Dias Volta     | Indica, em dias, o tempo necessário para percorrer do ponto de entrega do corregamento até local de origem do veículo.|

![alt text](../../img/orcamento_frete/orcamento_frete_04.png "Área de Dados de Veículo")

###Itens de Custo

Essa área é composta por outras cinco subáreas, **Depreciação de Bens**, **Manutenção**, **Combustível**, **Folha de Pagamento** e **Outras Despesas**, todas essas utilizadas para discriminar o valor de despesas referente a algum tipo de ítem ou serviço operacional. Essas subáreas são compostas por [Itens de Orçamento de Frete](../../orcamento_frete/cadastro/item_orcamento_frete).

- Depreciação de Bens: É referente ao valor médio que um determinado [tipo de veículo](../../veiculo/cadastro/tipo_veiculo) deprecia diariamente;
- Manutenção: É referente ao valor médio que um determinado [tipo de veículo](../../veiculo/cadastro/tipo_veiculo) gasta em manutenção;
- Combustível: Informa a média de combustível gasta por um determinado [tipo de veículo](../../veiculo/cadastro/tipo_veiculo), levando em consideração a distância a ser percorrida e o valor atual do combustível;
- Folha de Pagamento: Geralmente são despesas relacionadas aos gastos com as pessoas envolvidas com o frete, como por exemplo, os motoristas;
- Outras Despesas: São despesas gastas com serviços ???;

![alt text](../../img/orcamento_frete/orcamento_frete_05.png "Área de Itens de Custo")

###Impostos

Nessa área são dispostos percentuais dos tributos a serem pagos pela transição da carga.

Desses tributos vale ressaltar dois:

- PIS/CONFINS: Seu valor é obtido automaticamente conforme o valor vigente disposto nos registros de [Impostos Federais]();
- ICMS: Obtem um percentual relativo de acordo com a UF de origem e a UF de destino. Esses valores são definido na [Tabela de Alíquotas de ICMS]().

Os demais, são informados manualmente pelo usuário.

![alt text](../../img/orcamento_frete/orcamento_frete_06.png "Área de Impostos")

###Markup

O Markup, de forma resumida, é o controlador do preço de venda, levando em consideração o percentual das despesas e percentual do lucro.

É possível criar itens de markup através de [Itens de Orçamento de Frete](../../orcamento_frete/cadastro/item_orcamento_frete).

![alt text](../../img/orcamento_frete/orcamento_frete_07.png "Área de Markup")

###Valores do Orçamento

Com base em todas as outras áreas anteriores, quando todos os campos estiverem preenchidos, o sistema irá calcular um valor de sugestão para o orçamento de frete, entretanto, o usuário tem a possibilidade de alterar esse valor, o definindo no campo **Valor do Orçamento**. Além disso, nessa área também existe um campo informativo, para descrever o percentual do seguro sobre a carga.

![alt text](../../img/orcamento_frete/orcamento_frete_08.png "Área de Valores do Orçamento")

###Totais

Nessa área, apenas são exibidos os valores calculados de acordo com os percentuais das demais áreas. Nenhum desses campos é editado manualmente.

![alt text](../../img/orcamento_frete/orcamento_frete_09.png "Área de Totais")