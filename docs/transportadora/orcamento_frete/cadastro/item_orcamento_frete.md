#Item Orçamento Frete

Caminho: **Cadastro  >>  Item Orçamento Frete  >>  Manutenção de Item de Orçamento de Frete**

O conceito do cadastro dos itens de orçamento de frete é, basicamente, criar pequenas peças que serão utilizadas para representar as despesas no momento da solicitação de um [orçamento de frete](../../orcamento_frete).

Essas despesas são constiuídas pelas seguintes características:

| Campo              | Descrição                                                                                                     |
| ------------------ | ------------------------------------------------------------------------------------------------------------- |
| Descrição          | Descreve qual a despesa que está sendo representada.                                                          |
| Tipo Item          | É o tipo da despesa. Vale ressaltar que existem dois tipos de item que não são variáveis, **Depreciação** e **Manutenção**. Esses dois itens, sempre são relacionados com o [Tipo de Veículo](../../../veiculo/cadastro/tipo_veiculo), que, dentro do cenário do orçamento de frete, são os únicos elementos com essas características. |
| Vl. Sugestão       | Defineção de um valor médio para a despesa.                                                                   |
| Ativo              | Realiza o controle do para exibição do item no momento da solicitação de um orçamento de frete.               |
