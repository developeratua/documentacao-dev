!["Imagem não encontrada"](../../../img/transportadora/emissao_doc/emissao_cte/emissao_cte_com_lote_carregamento.png)

O lote de carregamento acontece quando a transportadora acorda com um cliente uma grande quantidade de determinado produto com uma tarifa fixa por tonelada.

Os principais objetivos de se emitir utilizando um lote de carregamento são:

- Agilizar o processo;
- Considerar valores previamente definidos;