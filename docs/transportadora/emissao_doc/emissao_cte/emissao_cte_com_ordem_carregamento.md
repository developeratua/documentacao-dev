!["Imagem não encontrada"](../../../img/transportadora/emissao_doc/emissao_cte/emissao_cte_com_ordem_carregamento.png)

A ordem de carregamento é um documento não fiscal. A Transportadora repassa este documento ao motorista, com uma autorização para que o motorista carregue em nome dela o produto da empresa do cliente remetente ou em seu consignatário expedidor.

Após carregar, o motorista retorna para a Agência onde será emitido o CT-e para que ele possa iniciar a viagem.

A emissão com ordem de carregamento tem por objetivo agilizar o processo, pois o sistema utiliza as informações que foram preenchidas previamente na ordem de carregamento.