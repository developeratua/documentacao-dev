**Caminho:** Emissão Doc. >> Emitir CTRC >> Emissão CTRC / CT-e

O principal objetivo do CT-e (Conhecimento de Transporte Eletrônico) é permitir que seja documentado, para fins fiscais, a prestação do serviço de transporte de cargas.

## Veículos envolvidos no frete

| Campo | Descrição |
| - | - |
| **Veículo** | Refere-se ao Veículo principal, quando o Veículo que fará o frete é composto pelo Cavalo + Carretas esse campo está se referindo ao Cavalo. Caso não seja o caso anterior, representa o Veículo como um todo. Toda emissão de um CT-e vai ter um Veículo preenchido visto que esse campo é obrigatório. |
| **Placa Carreta 1 / 2 / 3** | Referem-se ao Carretas do Cavalo, nem sempre vão existir visto que possuem Veículos onde não temos essa divisão entre Cavalo e Carretas. Exemplo: Caminhão Baú. |

## Pessoas envolvidas no frete

| Campo | Descrição |
| - | - |
| **Proprietário Posse** | Refere-se a pessoa que está em posse do Veículo, nem sempre a pessoa que estiver na ANTT estará em posse do Veículo. |
| **Proprietário Documento** | Refere-se a pessoa que está na ANTT para o Veículo. |
| **Remetente** | Refere-se a pessoa que vai ENVIAR a mercadoria a ser transportada. |
| **Destinatário** | Refere-se a pessoa que vai RECEBER a mercadoria a ser transportada. |
| **Consig. Rem./Expedidor** | Em alguns casos, a mercadoria não estará com o Remetente. Quando usado o campo Consig. Rem./Expedidor estamos assumindo que a mercadoria deve ser coletada com outra pessoa. |
| **Consig. Dest./Recebedor** | Em alguns casos, a mercadoria não deverá ser entregue diretamente ao Destinatário, sendo assim, devemos informar o Consig. Dest./Recebedor. |

## Produtos / Dados Nota Fiscal Transportada

Nessa área são informados os dados referente a mercadoria a ser transportada.

Geralmente os produtos serão preenchidos automaticamente com a utilização dos [Documento(s) Eletrônico(s)](emissao_cte_com_documento_eletronico.md).

## Tarifa

| Tipo | Descrição |
| - | - |
| **Valor Total do CTRC** | Quando não temos um cálculo específico, o usuário informa um valor fixo pelo valor do frete. |
| **Multiplica p/ Qtde. Unidades** | Quando usada essa opção, o sistema irá multiplicar o valor da tarifa pela soma de todas as "Qtde. Unidades" dos produtos. |
| **Multiplica p/ Ton (Peso/1000)** | Quando usada essa opção, o sistema irá multiplicar o valor da tarifa pela soma de todos os "Peso (Kg)" dos produtos. |
| **Multiplica p/ Km Rodado** | Quando usada essa opção, o sistema irá multiplicar o valor da tarifa pelo valor que estiver presente no campo "Km Rodado". |
| **Multiplica p/ Qtde Calculada (Peso/Peso Unid.)** | Quando usada essa opção, o sistema irá multiplicar o valor da tarifa pela soma de todos os "Peso (Kg)" dos produtos dividido pelo peso da "Espécie de Volume" selecionada nos produtos. |
| **Valor Total do CTRC / ALL-IN** | Quando não temos um cálculo específico, o usuário informa um valor fixo pelo valor do frete. Com esse tipo de tarifa, os valores de ICMS e pedágio não são considerados, sendo assim, o valor "Total do Serviço" vai ficar igual ao "Valor Tarifa Empresa". |

## ICMS

| Tipo | Descrição |
| - | - |
| **Isento** | É usado quando não temos incidência de ICMS no frete. |
| **Incluso** | É usado quando temos incidência de ICMS e o valor está incluso no valor do frete. |
| **+ ICMS** | É usado quando temos incidência de ICMS e o valor é um valor a parte ao frete. |


## Pedágio Empresa

| Tipo | Descrição |
| - | - |
| **Trajeto s/ Pedágio** | Acontece quando não temos nenhum pedágio no trajeto do frete. |
| **Incluso** | É usado quando o valor do pedágio já está incluso no valor do frete. |
| **Cobrar + Pedágio** | É usado quando o valor do pedágio é um valor a parte ao valor do frete. |

## Pedágio Motorista

| Tipo | Descrição |
| - | - |
| **Trajeto s/ Pedágio** | Acontece quando não temos nenhum pedágio no trajeto do frete. |
| **Incluso na Tarifa** | É usado quando o valor do pedágio já está incluso no valor do frete. |
| **Incluso na Tarifa => Pg Cartão/Cupom** | É usado quando o valor do pedágio já está incluso no valor do frete e esse valor será pago através do emissor de vale pedágio. |
| **+ Pedágio(Pago no Cartão/Cupom)** | É usado quando o valor do pedágio é um valor a parte ao valor do frete e esse valor será pago através do emissor de vale pedágio. |
| **+ Pedágio(Pagar no Adiantamento)** | É usado quando o valor do pedágio é um valor a parte ao valor do frete e esse valor será pago junto a parcela de adiantamento. |
| **+ Pedágio(Pagar no Saldo)** | É usado quando o valor do pedágio é um valor a parte ao valor do frete e esse valor será pago junto a parcela de saldo. |

## Carta Frete

A **Carta Frete** representa a maneira como o **Motorista** ou **Proprietário Doc.** vai receber pelo frete que está realizando.

Raramente a transportadora pagará o motorista em uma única parcela, normalmente a transportadora irá dividir o valor que o motorista tem para receber em uma ou mais parcelas de adiantamento mais a parcela de saldo.

| Tipo da Parcela | Descrição |
| - | - |
| Adiantamento | Referem-se as parcelas que o **Motorista** poderá trocar geralmente antes ou no decorrer do frete. |
| Saldo | Refere-se a parcela que o **Motorista** poderá trocar após realizar o frete. Nesse momento ele terá todos os comprovantes necessários para realizar a troca (comprovante de entrega, ticket da balança, etc...). |
| Combustível | Falta documentar. |
| Diária de viagem | Falta documentar. |
| Estadia de carregamento | Falta documentar. |

## Troca da carta frete sem Adm. Pgto. Eletrônico

Após autorizar o documento, o sistema disponibizará as vias de impressão das parcelas da carta frete.

Com as vias em mão, o motorista poderá trocar as parcelas conforme as datas de liberação estipuladas pela transportadora.

Ao chegar no posto, o atendente liberará o valor da parcela através do site do [https://consulta.maisfrete.com.br](https://consulta.maisfrete.com.br).

Ao informar a chave presente na via da parcela, o sistema fará todas as validações necessárias, e caso tudo esteja certo, liberará o valor acordado.

## Troca da carta frete com Adm. Pgto. Eletrônico

Falta documentar