![Imagem não encontrada](../../../img/transportadora/emissao_doc/emissao_cte/emissao_cte_com_documento_eletronico.png)

A emissão utilizando documentos eletrônicos é muito usada pois agiliza o processo e garante que as informações referente os produtos serão preenchidas corretamente.

O mais utilizado é o **Modelo 55 / NF-e**, pois refere-se as notas fiscais dos produtos.

Existem outros modelos, abaixo são listados mais alguns exemplos:

- Modelo 57 - CT-e
- Modelo 58 - MDF-e
- Modelo 65 - NFC-e