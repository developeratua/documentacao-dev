#Tipo Veículo Conjunto

Quando um caminhão vai para estrada, o usual é que o veículo esteja atrelado a um conjunto, caso esse possua um rodado que permita essa ação, para que seja possível o transporte de grandes cargas ou de cargas que necessitem de um tipo específico de transporte.

A estrutura de um tipo de conjunto é, geralmente, estruturada por um tipo de veículo de categoria **cavalo** e de um a três da categoria **carreta**.

Vale ressaltar que a informação adicionada no campo **Peso Máximo** é utilizada para validação no cadastro da [Ordem de Carregamento](), ou no momento da [emissão de CT-e](), caso esse não possua [Ordem de Carregamento]().

![alt text](../../../img/veiculo/cadastro/tipo_veiculo_conjunto.png "Tipo Veículo Conjunto")

