#Tipo Veículo

Como o próprio nome diz, essa parte do sistema é responsável por registrar os tipos do veículos utilizados no sistema, esses são mais tarde relacionados com um veículo específico, e também são necessários para a [movimentação de pneus](../../frota/pneu/#movimentacao)

##Manutenção de Tipo de Veículo

Caminho: **Cadastro  >>  Diversos  >>  Tipo de Veículo  >>  Manutenção de Tipo de Veículo**

Nessa área são informados os principáis dados de um tipo de veículo, ou seja suas principais características que o torna de um tipo específico.

| Campo                   | Descrição                                                                                                                           |
| ----------------------- | ------------------------------------------------------------------------------------------------------------------------------------|
| Tipo Veículo            | Nome do tipo veículo.                                                                                                               |
| Categoria               | Indica a categoria ao qual o veículo se encaixa, essa podendo ser **Caminhão/Cavalo**, **Carreta** e **Outros**, nessa última se encaixam veículos como carros, motos, etc...                                                                                                                    |
| Nr. Eixos               | Indica a quantidade de eixos do veículo.                                                                                            |
| Rodado                  | Indica um tipo de veículo tracionado.                                                                                               |
| Carroceria              | Indica o formato de construção da carroceria. Ex: **aberta** para cargas muito altas, ou **baú** para transporte de itens diversos. |

![alt text](../../../img/veiculo/cadastro/tipo_veiculo.png "Manutenção de Posição Pneu Tipo Veículo")

##Posição do Pneu

Em um cenário real, representa as posições em que os pneus serão alocados em um véiculo em relação aos seus eixos.

![alt text](../../../img/veiculo/cadastro/tipo_veiculo_posicao_pneu.png "Manutenção de Posição Pneu Tipo Veículo")

##Mapeamento do Pneu

Nessa tela o usuário demarca as posições dos pneus numa representação do tipo de veículo.

![alt text](../../../img/veiculo/cadastro/tipo_veiculo_pneu_posicao_mapeamento.png "Mapeamento de posição de pneus")

Os pneus com a cor predominante **laranja** indicam que essa posição já foi mapeada, enquanto os que estiverem em **cinza escuro** estão disponíveis para o mapeamento.

Para realizar o mapeamento, basta selecionar a posição e clicar sobre o pneu correspondente.

Vale ressaltar que não é possível alterar uma **categoria** de mapeamento do veículo, quando esse já possuir posições de pneus mapeadas.