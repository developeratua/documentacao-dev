jQuery.fn.toggleAttr = function(attr) {
	return this.each(function() {
	 var $this = $(this);
	 $this.attr(attr) ? $this.removeAttr(attr) : $this.attr(attr, attr);
	});
 };

 window.openedTabs = [];

function dataTypeHighLight() {
	
	var typeColor = [
		{dataType: "string", color:"#228b22"},
		{dataType: "int", color:"#00bfff"},
		{dataType: "float", color:"#1e90ff"},
		{dataType: "array", color:"#ff8c00"},
		{dataType: "boolean", color:"#b22222"},
		{dataType: "json", color:"#F1C40F"},
		{dataType: "object", color:"#2f4f4f"},
		{dataType: "date", color:"#A3247E"},
		{dataType: "callback", color:"#4B0082"},
		{dataType: "timestamp", color:"#AC1D00"},
		{dataType: "time", color:"#EA08E6"},
		{dataType: "null", color:"#808080"},
		{dataType: "false", color:"#E74C3C"},
		{dataType: "true", color:"#2471A3"}
	];
	
	var mainHtml = $('table');
	
	mainHtml.each((i, elTable) => {
		typeColor.forEach(el => {
			var rgx  = new RegExp('\\b' + el.dataType + '\\b', 'gmi');
			var repl = '<span style="color:' + el.color + ';">' +el.dataType+  ' </span>';

			$(elTable).html($(elTable).html().replace(rgx, repl));
		});
	});
}

function toggleCustomSubmenu()
{
	$('.customSubmenu').click(function(){
		$(this).toggleAttr('open');
		$(this).parent().siblings('ul').toggle();

		window.openedTabs.push($(this).attr('id'));

		localStorage.setItem('openedTabs', window.openedTabs);
	});
}

var css = `
	<style>
		.customSubmenu {cursor: pointer;}
		ul.subnav > li > span.caption-text ~ ul {display: none;}
	</style>
`;

$(document).ready(() => {
	$('head').append(css);

	dataTypeHighLight();
	toggleCustomSubmenu();

	var arTabs = localStorage.getItem('openedTabs').split(',') || [];

	arTabs.forEach((e, i) => {
		$(`#${e}`).trigger('click');
	});

	localStorage.setItem('openedTabs', null);
});
