# Bem-vindo a documentação da programação da Atua

---

Ao navegar nas abas você irá encotrar:

* Jaguar:
	* Objetos do framework Jaguar;
	* Classes úteis;
	* Funções úteis;
* Padrões:
	* Conjunto de boas práticas de desenvolvimento para manter um nível de qualidade, clareza e facilidade de manutenção nos códigos fonte.
* Tutoriais:
	* Material de auxilio na aprendizagem do framework Jaguar.